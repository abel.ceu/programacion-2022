package ejercicios.ejercicio11;

import java.math.BigDecimal;

public class PedidoLinea {
	private Integer numPedido;
	private String articulo;
	private Integer cantidad;
	private BigDecimal precio;
	public Integer getNumPedido() {
		return numPedido;
	}
	public void setNumPedido(Integer numPedido) {
		this.numPedido = numPedido;
	}
	public String getArticulo() {
		return articulo;
	}
	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public BigDecimal getPrecio() {
		return precio;
	}
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}
	@Override
	public String toString() {
		return "PedidoLinea [numPedido=" + numPedido + ", articulo=" + articulo + ", cantidad=" + cantidad + ", precio="
				+ precio + "]";
	}
	
	
	
}
