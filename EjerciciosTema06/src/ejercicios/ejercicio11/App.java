package ejercicios.ejercicio11;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class App {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		PedidosService service = new PedidosService();
		Integer opcionElegida = 0;
		do {
			try {
				opcionElegida = mostrarMenu(sc);
				if (opcionElegida == 1) {
					Pedido pedido = obtenerDatosPedido(sc);
					service.crearPedidoCompleto(pedido);
					System.out.println("Se ha insertado correctamente");
				}
				else if (opcionElegida == 2){
					Integer numPedido = obtenerNumeroPedido(sc);
					Pedido pedido = service.consultarPedidoCompleto(numPedido);
					System.out.println(pedido);
				}
				else if (opcionElegida == 3){
					Integer numPedido = obtenerNumeroPedido(sc);
					LocalDate fecha = obtenerFechaPedido(sc);
					service.actualizarDatosPedido(numPedido, fecha);
					System.out.println("Se ha actualizado correctamente");
				}
				else if (opcionElegida != 0) {
					System.out.println("Opci�n incorrecta.");
				}
			}
			catch(PedidoException | NotFoundException e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}
			catch(InputMismatchException e) {
				System.err.println("La opci�n de men� es num�rica");
			}
		}
		while(opcionElegida != 0);
		
		System.out.println("Bye!!");
		
	}

	private static Integer obtenerNumeroPedido(Scanner sc) {
		System.out.println("Dame n�mero pedido");
		Integer numPedido = sc.nextInt();
		sc.nextLine();
		return numPedido;
	}

	private static LocalDate obtenerFechaPedido(Scanner sc) {
		System.out.println("Dame fecha de pedido (dd/MM/yyyy)");
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		return LocalDate.parse(sc.nextLine(), format);
	}
	
	private static Pedido obtenerDatosPedido(Scanner sc) {
		Pedido pedido = new Pedido();
		pedido.setNumPedido(obtenerNumeroPedido(sc));
		
		System.out.println("Dame cliente del pedido");
		pedido.setCliente(sc.nextLine());
		
		pedido.setFechaPedido(obtenerFechaPedido(sc));
		
		System.out.println("Dame n�mero de art�culos");
		Integer cant = sc.nextInt();
		sc.nextLine();
		
		List<PedidoLinea> lineas = new ArrayList<>();
		for (int i = 0; i < cant; i++) {
			PedidoLinea linea = new PedidoLinea();
			System.out.println("Dame el articulo de la l�nea " + (i+1));
			linea.setArticulo(sc.nextLine());
			System.out.println("Dame la cantidad de articulos de la l�nea " + (i+1));
			linea.setCantidad(sc.nextInt());
			System.out.println("Dame el precio de articulo de la l�nea " + (i+1));
			linea.setPrecio(sc.nextBigDecimal());
			sc.nextLine();
			lineas.add(linea);
		}
		pedido.setLineas(lineas);
		return pedido;
		
	}

	private static Integer mostrarMenu(Scanner sc) {
		System.out.println("Elige una opci�n:");
		System.out.println("\t1. Crear pedido");
		System.out.println("\t2. Consultar pedido");
		System.out.println("\t3. Actualizar pedido");
		System.out.println("\t0. Salir");
		Integer opcion = sc.nextInt();
		sc.nextLine();
		return opcion;
	}
}
