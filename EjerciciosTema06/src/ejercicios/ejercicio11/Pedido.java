package ejercicios.ejercicio11;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;

public class Pedido {
	private Integer numPedido;
	private String cliente;
	private LocalDate fechaPedido;
	private List<PedidoLinea> lineas;
	private BigDecimal subtotal;
	private BigDecimal iva;
	private BigDecimal total;
	
	public Integer getNumPedido() {
		return numPedido;
	}
	public void setNumPedido(Integer numPedido) {
		this.numPedido = numPedido;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public LocalDate getFechaPedido() {
		return fechaPedido;
	}
	public void setFechaPedido(LocalDate fechaPedido) {
		this.fechaPedido = fechaPedido;
	}
	public List<PedidoLinea> getLineas() {
		return lineas;
	}
	public void setLineas(List<PedidoLinea> lineas) {
		this.lineas = lineas;
	}
	
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
	public void calcularTotales() {
		subtotal = BigDecimal.ZERO;
		for (PedidoLinea linea : lineas) {
			BigDecimal cantidad = new BigDecimal(linea.getCantidad());
			BigDecimal importe = cantidad.multiply(linea.getPrecio()).setScale(2, RoundingMode.HALF_UP);
			subtotal = subtotal.add(importe);
		}
		iva = subtotal.multiply(new BigDecimal("0.21")).setScale(2, RoundingMode.HALF_UP);
		total = subtotal.add(iva);
	}
	
	
	
	@Override
	public String toString() {
		return "Pedido [numPedido=" + numPedido + ", cliente=" + cliente + ", fechaPedido=" + fechaPedido + ", lineas="
				+ lineas + ", subtotal=" + subtotal + ", iva=" + iva + ", total=" + total + "]";
	}

	

}





