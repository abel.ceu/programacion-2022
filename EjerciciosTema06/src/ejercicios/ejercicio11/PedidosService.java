package ejercicios.ejercicio11;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import test.ProveedorConexiones;

public class PedidosService {

	public List<PedidoLinea> consultarLineasPedido(Integer numPedido) throws SQLException {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			List<PedidoLinea> lineas = new ArrayList<>();
			conn = new ProveedorConexiones().getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT * FROM PEDIDOS_LINEAS WHERE NUMERO_PEDIDO = " + numPedido;
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				PedidoLinea linea = new PedidoLinea();
				linea.setNumPedido(numPedido);
				linea.setArticulo(rs.getString("ARTICULO"));
				linea.setPrecio(rs.getBigDecimal("PRECIO_UNITARIO"));
				linea.setCantidad(rs.getInt("CANTIDAD"));
				lineas.add(linea);
			}
			return lineas;
		}
		finally {
			if (conn!=null) {
				conn.close();
			}
			if (stmt!=null) {
				stmt.close();
			}
		}
	}
	
	public Pedido consultarPedidoCompleto(Integer numPedido) throws NotFoundException, PedidoException {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = new ProveedorConexiones().getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT * FROM PEDIDOS WHERE NUMERO = " + numPedido;
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				Pedido pedido = new Pedido();
				pedido.setNumPedido(numPedido);
				pedido.setCliente(rs.getString("CLIENTE"));
				pedido.setFechaPedido(rs.getDate("FECHA_ENTREGA").toLocalDate());
				List<PedidoLinea> lineas = consultarLineasPedido(numPedido);
				pedido.setLineas(lineas);
				pedido.calcularTotales();
				return pedido;
			}
			throw new NotFoundException("No existe pedido con ese n�mero");
			
		}
		catch(SQLException e) {
			throw new PedidoException("Error al consultar pedido", e);
		}
		finally {
			try {
				stmt.close();
			}catch(Exception e) {}
			try {
				conn.close();
			}catch(Exception e) {}
		}
	}
	
	public void insertarLineaPedido(PedidoLinea linea, Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO PEDIDOS_LINEAS VALUES (?, ?, ?, ?)";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, linea.getNumPedido());
			stmt.setString(2, linea.getArticulo());
			stmt.setInt(3, linea.getCantidad());
			stmt.setBigDecimal(4, linea.getPrecio());
			stmt.execute();
		}
		finally {
			try {
				stmt.close();
			}catch(Exception e) {}
		}
	}
	
	
	public void crearPedidoCompleto(Pedido pedido) throws  PedidoException {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = new ProveedorConexiones().getConnection();
			conn.setAutoCommit(false);
			String sql = "INSERT INTO PEDIDOS VALUES (?, ?, ?)";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, pedido.getNumPedido());
			stmt.setString(2, pedido.getCliente());
			stmt.setDate(3, Date.valueOf(pedido.getFechaPedido()));
			stmt.execute();
			
			for (PedidoLinea linea : pedido.getLineas()) {
				linea.setNumPedido(pedido.getNumPedido());
				insertarLineaPedido(linea, conn);
			}
			conn.commit();
		}
		catch(SQLException e) {
			try {
				conn.rollback();
			}catch(SQLException e2) {
				throw new PedidoException("Error al insertar pedido y no se ha podido deshacer transacci�n. La BBDD puede estar inconsistente", e);
			}
			throw new PedidoException("Error al insertar el pedido", e);
		}
		finally {
			try {
				stmt.close();
			}catch(Exception e) {}
			try {
				conn.close();
			}catch(Exception e) {}
		}
	}

	public void actualizarDatosPedido(Integer numPedido, LocalDate fecha) throws NotFoundException, PedidoException {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = new ProveedorConexiones().getConnection();
			String sql = "UPDATE PEDIDOS SET FECHA_ENTREGA = ? WHERE NUMERO = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setDate(1, Date.valueOf(fecha));
			stmt.setInt(2, numPedido);
			Integer cantActualizada = stmt.executeUpdate();
			if (cantActualizada == 0) {
				throw new NotFoundException("No existe el pedido con n�mero " + numPedido);
			}
		}
		catch(SQLException e) {
			throw new PedidoException("Error al actualizar fecha del pedido", e);
		}
		finally {
			try {
				stmt.close();
			}catch(Exception e) {}
			try {
				conn.close();
			}catch(Exception e) {}
		}
	}
}






