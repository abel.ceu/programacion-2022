package ejercicios.ejercicio01al09;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Test {

	public static void main(String[] args) {
		List<Persona> personas = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			Persona p = new Persona();
			p.setDni(i+"0000000X");
			p.setNombre("Blas " + i);
			if (i == 2) {
				p.setApellidos("Blau dsk�afl sasdfasd flk�asjd f�lkajsd �flkajsd f�lkaj sd�flajsdl�fjasdl�fjl�asdjfl�asdkjf �alsdj f�lasdjkf l�askjdf �lasjdf�lasdjkf �laskjd f�lasdjf �laskdjf �lasdj f�lasjdf l�asdjf �laskdjf a�lsdkjf �alsdkjf �lasjdf �lasdjf �laskdj fl�asdj f sadfjasdlkf asd�fkasd f�asdlfk asdlasdf sdafasfda asd�fjkasd asdf asd�faksjdf �asdlkjf a�slkdjf �laskjdf �laksdj f�lasdjkf �laksjd d�lfj l�asdj f�lasdj f�lasdj f�lasjd f�laksdjf�lasjdkf�lasdjf�lajsdl�fjasd�lfjas�ldfja�lsdjfl�asdjf�lasdjf�lasjdf�lasdjk " + i);
			}
			else {
				p.setApellidos("Blau " + i);
			}
			p.setFechaNacimiento(LocalDate.now());
			personas.add(p);
		}
		
		PersonasService service = new PersonasService();
		try {
			service.insertarPersonas(personas);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		

	}

}
