package ejercicios.ejercicio01al09;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		PersonasService service = new PersonasService();
		Integer opcion = 0;
		do {
			try {
				imprimirMenu();
				opcion = sc.nextInt();
				sc.nextLine();
				switch (opcion) {
				case 1:
					buscarPersona(sc, service);
					break;
				case 2:
					buscarPersonaFiltro(sc, service);
					break;
				case 3:
					crearPersona(sc, service);
					break;
				case 4:
					actualizarPersona(sc, service);
					break;
				case 5:
					borrarPersona(sc, service);
					break;
				case 0:
					System.out.println("Bye!!");
					break;
				default:
					System.out.println("La opci�n indicada no es v�lida");
					break;
				}
			} catch (SQLException e) {
				System.err.println("Error al conectar o trabajar con BBDD");
			} catch (PersonaNotFoundException e) {
				System.err.println(e.getMessage());
			}

		} while (opcion != 0);

		sc.close();
	}

	
	private static void actualizarPersona(Scanner sc, PersonasService service) throws SQLException {
		Persona persona = obtenerPersona(sc);
		try {
			persona.validar();
			service.actualizarPersona(persona);
		}
		catch(PersonaNotValidException e) {
			System.err.println("Los datos indicados no son correctos. " + e.getMessage());
		} catch (PersonaNotFoundException e) {
			System.err.println(e.getMessage());
		}
	}
	
	private static void borrarPersona(Scanner sc, PersonasService service) throws SQLException {
		System.out.println("Dame el DNI");
		String dni = sc.nextLine();
		try {
			service.borrarPersona(dni);
		} catch (PersonaNotFoundException e) {
			System.err.println(e.getMessage());
		}
	}
	
	
	private static void crearPersona(Scanner sc, PersonasService service) throws SQLException {
		Persona nueva = obtenerPersona(sc);
		try {
			nueva.validar();
			service.consultarPersona(nueva.getDni());
			System.err.println("La persona ya existe en la BBDD");
		} catch (PersonaNotValidException e) {
			System.err.println("Los datos no son correctos. " + e.getMessage());
		} catch (PersonaNotFoundException e) {
			service.insertarPersona(nueva);
		}
	}


	private static Persona obtenerPersona(Scanner sc) {
		Persona nueva = new Persona();
		System.out.println("Dime el DNI");
		nueva.setDni(sc.nextLine());
		System.out.println("Dime el nombre");
		nueva.setNombre(sc.nextLine());
		System.out.println("Dime el apellido");
		nueva.setApellidos(sc.nextLine());
		System.out.println("Dime la fecha de nacimiento (dd/mm/yyyy)");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		nueva.setFechaNacimiento(LocalDate.parse(sc.nextLine(), formatter));
		return nueva;
	}

	private static void buscarPersonaFiltro(Scanner sc, PersonasService service)
			throws SQLException, PersonaNotFoundException {
		System.out.println("Dime filtro para buscar");
		String filtro = sc.nextLine();
		List<Persona> lista = service.buscarPersonas(filtro);
		for (Persona p : lista) {
			System.out.println(p);
		}
	}

	private static void buscarPersona(Scanner sc, PersonasService service)
			throws SQLException, PersonaNotFoundException {
		System.out.println("Indica el DNI");
		String dni = sc.nextLine();
		Persona persona = service.consultarPersona(dni);
		System.out.println(persona);
	}

	public static void imprimirMenu() {
		System.out.println("Elige la opci�n deseada:");
		System.out.println("\t(1) Consultar persona por DNI");
		System.out.println("\t(2) Buscar personas por filtro");
		System.out.println("\t(3) Insertar persona");
		System.out.println("\t(4) Actualizar persona");
		System.out.println("\t(5) Borrar persona");
		System.out.println("\t(0) Salir");
	}

}
