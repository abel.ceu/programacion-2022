package ejercicios.ejercicio01al09;

import java.time.LocalDate;
import java.time.Period;

public class Persona {
	private String dni;
	private String nombre;
	private String apellidos;
	private LocalDate fechaNacimiento;
	
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	@Override
	public String toString() {
		return "Persona [dni=" + dni + ", nombre=" + nombre + ", apellidos=" + apellidos + ", fechaNacimiento="
				+ fechaNacimiento + "]";
	}
	
	public Boolean isValido() {
		if (dni == null || nombre == null || apellidos == null || fechaNacimiento == null) {
			return false;
		}
		if (dni.isBlank() || nombre.isBlank() || apellidos.isBlank()) {
			return false;
		}
		if (dni.length() > 50 || nombre.length() > 50 || apellidos.length() > 100) {
			return false;
		}
		return true;
	}
	
	public void validar() throws PersonaNotValidException {
		if (dni == null || nombre == null || apellidos == null || fechaNacimiento == null) {
			throw new PersonaNotValidException("Los datos no pueden ser null");
		}
		if (dni.isBlank() || nombre.isBlank() || apellidos.isBlank()) {
			throw new PersonaNotValidException("Los datos no pueden ser vac�os");
		}
		if (dni.length() > 50 || nombre.length() > 50 || apellidos.length() > 100) {
			throw new PersonaNotValidException("Los datos no pueden exceder el tama�o m�ximo");
		}
	}
	
	public boolean isMayorEdad() {
		LocalDate ahora = LocalDate.now();
		Period periodo = fechaNacimiento.until(ahora);
		return periodo.getYears() >= 18;
	}
	
	
	
}
