package ejercicios.ejercicio01al09;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import test.ProveedorConexiones;

public class PersonasService {

	public Persona consultarPersona(String dni) throws SQLException, PersonaNotFoundException {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = new ProveedorConexiones().getConnection();
			stmt = conn.createStatement();
			String sql = "SELECT * FROM PERSONAS WHERE DNI = '" + dni + "'";
			rs = stmt.executeQuery(sql);
			if(rs.next()) {
				return getPersona(rs);
			}
			throw new PersonaNotFoundException("No existe la persona con dni " + dni);
		}
		finally {
			if (stmt!=null) {
				stmt.close();
			}
			if (conn!=null) {
				conn.close();
			}
		}
		
	}
	
	public void insertarPersonasSinReutilizarCodigo(List<Persona> personas) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = new ProveedorConexiones().getConnection();
			String sql = "INSERT INTO PERSONAS VALUES (?, ?, ?, ?)";
			stmt = conn.prepareStatement(sql);
			conn.setAutoCommit(false);
			for (Persona persona : personas) {
				stmt.setString(1, persona.getDni());
				stmt.setString(2, persona.getNombre());
				stmt.setString(3, persona.getApellidos());
				stmt.setDate(4, Date.valueOf(persona.getFechaNacimiento()));
				stmt.execute();
			}
			conn.commit();
		}
		catch(SQLException e) {
			conn.rollback();
			throw e;
		}
		finally {
			if (stmt!=null) {
				stmt.close();
			}
			if (conn!=null) {
				conn.close();
			}
		}
	}
	
	public void insertarPersonas(List<Persona> personas) throws SQLException {
		Connection conn = null;
		try {
			conn = new ProveedorConexiones().getConnection();
			conn.setAutoCommit(false);
			for (Persona persona : personas) {
				insertarPersona(persona, conn);
			}
			conn.commit();
		}
		catch(SQLException e) {
			conn.rollback();
			throw e;
		}
		finally {
			if (conn!=null) {
				conn.close();
			}
		}
	}
	
	public void insertarPersona(Persona persona) throws SQLException {
		Connection conn = null;
		try {
			conn = new ProveedorConexiones().getConnection();
			insertarPersona(persona, conn);
		}
		finally {
			if (conn!=null) {
				conn.close();
			}
		}
		
	}
	
	private void insertarPersona(Persona persona, Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO PERSONAS VALUES (?, ?, ?, ?)";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, persona.getDni());
			stmt.setString(2, persona.getNombre());
			stmt.setString(3, persona.getApellidos());
			stmt.setDate(4, Date.valueOf(persona.getFechaNacimiento()));
			
			stmt.execute();
		}
		finally {
			if (stmt!=null) {
				stmt.close();
			}
		}
		
	}
	
	public void actualizarPersona(Persona persona) throws SQLException, PersonaNotFoundException {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = new ProveedorConexiones().getConnection();
			String sql = "UPDATE PERSONAS SET NOMBRE = ?, APELLIDOS = ?, FECHA_NACIMIENTO = ? WHERE DNI = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, persona.getNombre());
			stmt.setString(2, persona.getApellidos());
			stmt.setDate(3, Date.valueOf(persona.getFechaNacimiento()));
			stmt.setString(4, persona.getDni());
			
			Integer cantActualizada = stmt.executeUpdate();
			if (cantActualizada == 0) {
				throw new PersonaNotFoundException("No existe persona con el dni " + persona.getDni());
			}
		}
		finally {
			if (stmt!=null) {
				stmt.close();
			}
			if (conn!=null) {
				conn.close();
			}
		}
		
	}
	
	public void borrarPersona(Connection conn, String dni) throws SQLException, PersonaNotFoundException {
		PreparedStatement stmt = null;
		try {
			String sql = "DELETE FROM PERSONAS WHERE DNI = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, dni);
			
			Integer cantBorrada = stmt.executeUpdate();
			if (cantBorrada == 0) {
				throw new PersonaNotFoundException("No existe persona con el dni " + dni);
			}
		}
		finally {
			if (stmt!=null) {
				stmt.close();
			}
		}
		
	}
	
	
	public void borrarPersona(String dni) throws SQLException, PersonaNotFoundException {
		Connection conn = null;
		try {
			conn = new ProveedorConexiones().getConnection();
			borrarPersona(conn, dni);
		}
		finally {
			if (conn!=null) {
				conn.close();
			}
		}
		
	}
	
	public List<Persona> buscarPersonas(String filtro) throws SQLException, PersonaNotFoundException {
		List<Persona> lista = new ArrayList<>();
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = new ProveedorConexiones().getConnection();
			String sql = "SELECT * FROM PERSONAS WHERE NOMBRE LIKE ? OR APELLIDOS LIKE ?";
			//String sql = "SELECT * FROM PERSONAS WHERE NOMBRE LIKE '%" + filtro + "%' OR APELLIDOS LIKE '%" + filtro + "%'"; // con statement normal
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, "%" + filtro + "%");
			stmt.setString(2, "%" + filtro + "%");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Persona persona = getPersona(rs);
				lista.add(persona);
			}
			if (lista.isEmpty()) {
				throw new PersonaNotFoundException("No hay personas con el filtro de nombre o apellidos " + filtro);
			}
			return lista;
		}
		finally {
			if (stmt!=null) {
				stmt.close();
			}
			if (conn!=null) {
				conn.close();
			}
		}
		
	}

	
	public Integer borrarPersonasA() throws SQLException {
		Connection conn = null;
		try {
			conn = new ProveedorConexiones().getConnection();
			// 1. Consultar todas las personas
			List<Persona> personas;
			try {
				personas = buscarPersonas("");
			} catch (PersonaNotFoundException e1) {
				// Si no hay personas, devolvemos 0 porque no borramos nada
				return 0;
			}
			
			// 2. Recorrerlas para ver cu�les son mayores de edad
			Integer personasBorradas = 0;
			for (Persona persona : personas) {
				// 3. Si es mayor de edad --> Borrar
				if (persona.isMayorEdad()) {
					try {
						borrarPersona(conn, persona.getDni());
						personasBorradas++;
					} catch (PersonaNotFoundException e) {
						// No contamos las que no existen
					}
				}
			}
			
			// 4. Devolver total borrado
			conn.commit();
			return personasBorradas;
		}
		catch(SQLException e) {
			conn.rollback();
			throw e;
		}
		finally {
			if (conn!=null) {
				conn.close();
			}
		}
	}

	public Integer borrarPersonasB() throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = new ProveedorConexiones().getConnection();
			String sql = "DELETE FROM PERSONAS WHERE FECHA_NACIMIENTO <= ? ";
			stmt = conn.prepareStatement(sql);
			LocalDate fechaLimite = LocalDate.now().minusYears(18); // justo hace 18 a�os
			stmt.setDate(1, Date.valueOf(fechaLimite));
			
			return stmt.executeUpdate();
		}
		finally {
			if (stmt!=null) {
				stmt.close();
			}
			if (conn!=null) {
				conn.close();
			}
		}
	}

	private Persona getPersona(ResultSet rs) throws SQLException {
		Persona persona = new Persona();
		persona.setDni(rs.getString("dni"));
		persona.setNombre(rs.getString("nombre"));
		persona.setApellidos(rs.getString("apellidos"));
		persona.setFechaNacimiento(rs.getDate("fecha_nacimiento").toLocalDate());
		return persona;
	}
	
}





