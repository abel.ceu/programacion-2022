package ejercicios.ejercicio01al09;

public class PersonaNotValidException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3549390935158032634L;

	public PersonaNotValidException() {
	}

	public PersonaNotValidException(String message) {
		super(message);
	}

	public PersonaNotValidException(Throwable cause) {
		super(cause);
	}

	public PersonaNotValidException(String message, Throwable cause) {
		super(message, cause);
	}

	public PersonaNotValidException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
