package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ProveedorConexiones {

	public Connection getConnection() throws SQLException {
		String driver = "org.mariadb.jdbc.Driver";
		String usuario = "programacion";
		String password = "programacion";
		String url = "jdbc:mariadb://localhost:3306/programacion";
		try {
			Class.forName(driver);
			return DriverManager.getConnection(url, usuario, password);
		}
		catch(ClassNotFoundException e) {
			System.err.println("No se ha encontrado la clase del driver: " + driver);
			throw new SQLException("No se ha encontrado la clase del driver", e);
		}
	}

}
