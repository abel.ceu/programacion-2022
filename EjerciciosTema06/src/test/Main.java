package test;

import java.sql.SQLException;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		MascotasService service = new MascotasService();
		try {
			Mascota m = new Mascota();
			m.setCodigo("911");
			m.setNombre("rafa");
			service.insertarMascota(m);

			List<Mascota> lista = service.consultarMascotas();
			System.out.println("Estas son todas las mascotas que hay en la BBDD");
			for (Mascota mascota : lista) {
				System.out.println(mascota);
			}
			
			
		} catch (SQLException e) {
			System.err.println("Algo ha ido mal... revisa el temita");
			e.printStackTrace();
		}
		
	}

}
