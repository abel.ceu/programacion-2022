package ejercicio11;

public class Ejercicio11 {
	public static void main(String[] args) {
		Integer[][] tabla = new Integer[5][10];
		
		Integer contador = 1;
		for (int i = 0; i < tabla.length; i++) {
			Integer[] arrayInterno = tabla[i];
			for (int j = 0; j < arrayInterno.length; j++) {
				arrayInterno[j] = contador;
				contador++;
			}
		}
		
		for (int i = 0; i < tabla.length; i++) {
			Integer[] arrayInterno = tabla[i];
			for (int j = 0; j < arrayInterno.length; j++) {
				System.out.print(arrayInterno[j] + "\t");
			}
			System.out.println();
		}
		
		
	}
}
