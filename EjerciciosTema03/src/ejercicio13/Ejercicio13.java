package ejercicio13;

public class Ejercicio13 {
	public static void main(String[] args) {
		Integer[][] tabla = new Integer[10][10];
		
		
		for (int i = 0; i < tabla.length; i++) {
			Integer[] arrayInterno = tabla[i];
			for (int j = 0; j < arrayInterno.length; j++) {
				arrayInterno[j] = (i+1)*(j+1);
			}
		}
		
		for (int i = 0; i < tabla.length; i++) {
			Integer[] arrayInterno = tabla[i];
			for (int j = 0; j < arrayInterno.length; j++) {
				System.out.print(arrayInterno[j] + "\t");
			}
			System.out.println();
		}
		
		
	}
}
