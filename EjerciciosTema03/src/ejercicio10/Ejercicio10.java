package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Integer numero;
		do {
			System.out.println("Dime n�mero mayor a 2");
			numero = scanner.nextInt();
		}
		while(numero <= 2);
		
		Integer[] fibonacci = new Integer[numero];
		
		// llenar el array con la serie
		fibonacci[0] = 0;
		fibonacci[1] = 1;
		
		for (int x = 2; x < fibonacci.length; x++) {
			fibonacci[x] = fibonacci[x-1] + fibonacci[x-2];
		}
		
		
		// imprimir
		for (int i = 0; i < fibonacci.length; i++) {
			System.out.print(fibonacci[i] + " ");
		}
		
		
		scanner.close();
	}
}
