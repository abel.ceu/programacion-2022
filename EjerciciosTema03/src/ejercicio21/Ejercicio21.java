package ejercicio21;

public class Ejercicio21 {


	public static void main(String[] args) {
		
		Double[] numeros = new Double []{5.4, 3.1, 9.0, 4.7};

		for (Double numero : numeros) {
			System.out.println(numero);
		}
		
		
		Double suma = 0.0;
		for (Double numero : numeros) {
			suma = suma + numero;
		}
		
		System.out.println("La suma total es " + suma);
		
	}
	
}
