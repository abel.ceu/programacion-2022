package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Cu�ntos n�meros quieres");
		Integer total = scanner.nextInt();
		Integer[] numeros = new Integer[total];
		
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Dime el n�mero " + (i+1));
			Integer numero = scanner.nextInt();
			numeros[i] = numero;
		}
		
		System.out.println("Estos son tus n�meros");
		for (int i = numeros.length-1; i >= 0; i--) {
			System.out.println(numeros[i]);
		}
		
		
		scanner.close();
	}
}
