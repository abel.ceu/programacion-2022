package ejercicio20;

import java.util.Random;
import java.util.Scanner;

public class Ejercicio20 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		// Elegimos palabra al azar
		String[] palabras = new String[] { "terremoto", "culinario", "azulejo", "tenedor", "saltamontes", "carretilla",
				"molinero", "sofisticado", "teclado", "primavera" };
		Random random = new Random();
		Integer index = random.nextInt(10);
		String palabraSecreta = palabras[index];
		
		// Nos creamos array con las letras que hay que averiguar
		String[] original = palabraSecreta.split("");
		
		// Creamos el array con las letras que el usuario ha averiguado
		String[] tablero = new String[original.length];
		tablero[0] = original[0];
		for (int i = 1; i < tablero.length; i++) {
			tablero[i] = "_";
		}

		Integer vidas = 10;
		Boolean iguales = false;
		do {
			// 1. Imprimir tablero
			for (int i = 0; i < tablero.length; i++) {
				System.out.print(tablero[i] + " ");
			}
			System.out.println();
			
			// 2. Pedir letra
			System.out.println("Dame una letra");
			String letra = scanner.nextLine();

			// 3. Comprobar si la letra existe en la palabra
			Boolean acertado = false;
			for (int i = 1; i < original.length; i++) {
				if (original[i].equals(letra)) {
					tablero[i] = letra;
					acertado = true;
				}

			}

			if (acertado) { // 4. Si ha acertado, vemos si ya ha terminado comparando los dos arrays
				iguales = true;
				for (int i = 0; i < original.length; i++) {
					if (!original[i].equals(tablero[i])) {
						iguales = false;
						break;
					}
				}
			} else { // 4. Si ha fallado, actualizamos n�mero de vidas
				vidas--;
				System.out.println("Esa letra no est�. Te quedan " + vidas + " vidas");
			}

		} while (vidas > 0 && !iguales); // repetiremos mientras que haya suficientes vidas y que no haya completado el tablero

		if (vidas == 0) {
			System.out.println("Te has quedado sin vidas... m�s suerte la prxima vez");
		}
		else {
			System.out.println("Bravo!! Tu palabra era " + palabraSecreta);
		}

		scanner.close();
	}




}
