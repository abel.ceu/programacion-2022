package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dame una URL");
		String url = scanner.nextLine();
		
		Integer indiceBarra = url.indexOf("//");
		Integer indicePunto1 = url.indexOf(".");
		Integer indicePunto2 = url.lastIndexOf(".");
		
		if (indiceBarra == -1 || indicePunto1 == -1 || indicePunto1 == indicePunto2) {
			System.out.println("El formato de la URL no es correcta");
		}
		else {
			String[] partesPrincipales = url.split("//");
			String segundaParte = partesPrincipales[1];
			String[] partesSecundarias = segundaParte.split("\\.");
			
			
			System.out.println(partesPrincipales[0] + "//");
			
			for (String parte : partesSecundarias) {
				System.out.println(parte);
			}
			
		}
		
		scanner.close();
		
		
		
			
		
		
		
	}
	
	
}
