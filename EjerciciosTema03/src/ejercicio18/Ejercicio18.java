package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dime un n�mero");
		Integer numero = scanner.nextInt();
		
		String[] digitos = numero.toString().split("");
		
		
		Boolean capicua = true;
		for (int ini = 0, fin = digitos.length-1; ini < digitos.length; ini++, fin--) {
			if (!digitos[ini].equals(digitos[fin])) {
				System.out.println("No es capic�a");
				capicua = false;
				break;
			}
		}
		if (capicua) {
			System.out.println("Es capic�a!!");
		}
		
		scanner.close();
	}
	
	
}
