package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Cu�ntos n�meros quieres");
		Integer total = scanner.nextInt();
		Integer[] numeros = new Integer[total];
		
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Dime el n�mero " + (i+1));
			Integer numero = scanner.nextInt();
			numeros[i] = numero;
		}

		System.out.println("Estos son tus n�meros");
		for (int i = 0; i < numeros.length; i++) {
			System.out.println(numeros[i]);
		}

		Integer izda = 0;
		Integer dcha = numeros.length-1;
		while(izda < dcha) {
			Integer aux = numeros[izda];
			numeros[izda] = numeros[dcha];
			numeros[dcha] = aux;
			izda ++;
			dcha --;
		}
		
		System.out.println("Estos son tus n�meros");
		for (int i = 0; i < numeros.length; i++) {
			System.out.println(numeros[i]);
		}
		
		
		
		scanner.close();
	}
}
