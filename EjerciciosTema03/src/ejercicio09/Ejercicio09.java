package ejercicio09;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Cu�ntos n�meros quieres");
		Integer total = scanner.nextInt();
		Integer[] numeros = new Integer[total];
		
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Dime el n�mero " + (i+1));
			Integer numero = scanner.nextInt();
			numeros[i] = numero;
		}
		
		System.out.println("Estos son tus n�meros");
		for (int i = 0; i < numeros.length; i++) {
			System.out.println(numeros[i]);
		}
		
		for (int i = 0; i < numeros.length; i++) {
			Integer minimo = numeros[i];
			Integer posMinimo = i;
			for (int j = i; j < numeros.length; j++) {
				if (minimo > numeros[j]) {
					minimo = numeros[j];
					posMinimo = j;
				}
			}
			Integer aux = numeros[i];
			numeros[i] = numeros[posMinimo];
			numeros[posMinimo] = aux;
		}
		
		System.out.println("Estos son tus n�meros ordenados");
		for (int i = 0; i < numeros.length; i++) {
			System.out.println(numeros[i]);
		}
		
		scanner.close();
	}
}
