package ejercicio16;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dime una palabra");
		String palabra = scanner.nextLine();
		
		String[] letras = palabra.split("");
		
		for (int i = letras.length-1; i >= 0; i--) {
			System.out.print(letras[i]);
		}
		
		scanner.close();
	}
	
	
}
