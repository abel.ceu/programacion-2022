package ejercicio19;

import java.util.Scanner;

public class Ejercicio19 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dime una frase");
		String frase = scanner.nextLine();
		
		String[] palabras = frase.split(" ");
		System.out.println("Hay " + palabras.length + " palabras");
		
		String[] letras = frase.split("");
		Integer contador = 0;
		for (String letra : letras) {
			if (letra.equals("a")) {
				contador++;
			}
		}
		System.out.println("Hay " + contador + " Aes");
		
		
		
		
		scanner.close();
	}
	
	
}
