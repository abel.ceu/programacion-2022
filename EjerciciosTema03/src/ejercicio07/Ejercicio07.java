package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Cu�ntos n�meros quieres");
		Integer total = scanner.nextInt();
		Integer[] numeros = new Integer[total];
		
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Dime el n�mero " + (i+1));
			Integer numero = scanner.nextInt();
			numeros[i] = numero;
		}
		
		System.out.println("Estos son tus n�meros");
		for (int i = 0; i < numeros.length; i++) {
			System.out.println(numeros[i]);
		}

		
		Integer[] numerosInversa = new Integer[numeros.length];
		Integer x = numeros.length-1;
		for (int i = 0; i < numerosInversa.length; i++) {
			numerosInversa[i] = numeros[x];
			x--;
		}
		
		
		scanner.close();
	}
}
