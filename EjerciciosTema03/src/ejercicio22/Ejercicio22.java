package ejercicio22;

import java.util.Scanner;

public class Ejercicio22 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dime una palabra");
		String palabra = scanner.nextLine();
		
		String[] letras = palabra.split("");
		for (String letra : letras) {
			System.out.println(letra);
		}
		
		scanner.close();
		
	}

}
