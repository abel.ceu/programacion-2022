package ejercicio01;

public class Ejercicio01 {

	public static void main(String[] args) {
		Integer[] array = new Integer[15];
		
		// 1. Llenar el array con los pares del 1 al 30
		Integer x = 2;
		for (int i = 0; i < array.length; i++) {
			array[i] = x;
			x += 2;
		}
		
		// Alternativa 1
//		Integer posicion = 0;
//		for (int i = 1; i <= 30; i++) {
//			if (i%2 == 0) {
//				array[posicion] = i;
//				posicion++;
//			}
//		}
		
		// Alternativa 2
//		array[0] = 2;
//		for (int i = 1; i < array.length; i++) {
//			array[i] = array[i-1] + 2 ;
//		}
		
		
		// 2. Imprimir el array
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		
		
		
	}
	
	
}
