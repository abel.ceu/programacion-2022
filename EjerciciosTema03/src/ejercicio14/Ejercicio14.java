package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dame tu nombre completo");
		String nombreCompleto = scanner.nextLine();
		
		nombreCompleto = nombreCompleto.trim(); // opcional para evitar que el usuario nos ponga espacios adicionales
		
		String[] partesNombre = nombreCompleto.split(" ");
		
//		for (String parte : partesNombre) {
//			System.out.println(parte);
//		}
		
		String nombre = partesNombre[0];
		String apellido1 = partesNombre[1];
		String apellido2 = partesNombre[2];
		
		System.out.println("Nombre: " + nombre);
		System.out.println("Apellido 1: " + apellido1);
		System.out.println("Apellido 2: " + apellido2);
		
		
		scanner.close();
		
		
	}
	
	
}
