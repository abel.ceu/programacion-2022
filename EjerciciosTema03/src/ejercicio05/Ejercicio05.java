package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Cu�ntos n�meros quieres");
		Integer total = scanner.nextInt();
		Integer[] numeros = new Integer[total];
		
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Dime el n�mero " + (i+1));
			Integer numero = scanner.nextInt();
			numeros[i] = numero;
		}
		
		
		Integer suma = 0;
		Integer mayor = numeros[0];
		Integer posicionDelMayor = 0; 
		Integer menor = numeros[0];
		Integer posicionDelMenor = 0; 
		for (int i = 0; i < numeros.length; i++) {
			suma = suma + numeros[i];
			if (mayor < numeros[i]) {
				mayor = numeros[i];
				posicionDelMayor = i;
			}
			if (menor > numeros[i]) {
				menor = numeros[i];
				posicionDelMenor = i;
			}
			
			
		}
		System.out.println("La media es: " + (suma/numeros.length));
		System.out.println("El mayor es: " + mayor + " y est� en la posici�n " + posicionDelMayor);
		System.out.println("El menor es: " + menor + " y est� en la posici�n " + posicionDelMenor);
		
		
		
		System.out.println("Estos son tus n�meros");
		for (int i = 0; i < numeros.length; i++) {
			System.out.println(numeros[i]);
		}
		
		
		scanner.close();
	}
}
