package ejercicio17;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dime una frase");
		String frase = scanner.nextLine();
		
		// Obtengo array con todas las palabras
		String[] palabras = frase.split(" ");
		
		// Imprimo la primera palabra en min�sculas
		System.out.print(palabras[0].toLowerCase());
		
		// Recorremos el resto de palabras (desde la 1... en adelante)
		for (int i = 1; i < palabras.length; i++) {
			// Por cada palabra...
			String palabra = palabras[i];
			
			// Obtenemos todas las letras de esa palabra
			String[] letras = palabra.split("");
			
			// Imprimimos la primera letra en may�sculas
			System.out.print(letras[0].toUpperCase());
			
			// Recorremos el resto de letras de esa palabra (desde la 1... en adelante)
			for (int j = 1; j < letras.length; j++) {
				// Imprimo cada letra en min�sculas
				System.out.print(letras[j].toLowerCase());
			}
		}
		
		
		
		scanner.close();
	}
	
	
}
