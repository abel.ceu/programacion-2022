package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String a, b;
		
		do {
			System.out.println("Dime una palabra");
			a = scanner.nextLine();
			a = a.trim();

			System.out.println("Dime otra palabra");
			b = scanner.nextLine();
			b = b.trim();
			
		} while (! a.equalsIgnoreCase(b));

		System.out.println("Muchas gracias!!");

		scanner.close();

	}
}
