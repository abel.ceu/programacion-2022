package ejercicio19;

import java.util.Scanner;

public class Ejercicio19 {

	private static final double PORCENTAJE_EXENTO = 0.0;
	private static final double PORCENTAJE_NORMAL = 0.21;
	private static final double PORCENTAJE_REDUCIDO = 0.10;
	private static final double PORCENTAJE_SUPERREDUCIDO = 0.04;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dime el precio sin iva");
		Double precioSinIva = scanner.nextDouble();
		System.out.println("Dime el tipo de IVA (1:Normal 2:Reducido 3:Superreducido 4:Exento)");
		Integer tipoIva = scanner.nextInt();
		
		Double porcentaje = PORCENTAJE_EXENTO;
		
		switch (tipoIva) {
		case 1:
			porcentaje = PORCENTAJE_NORMAL;
			break;
		case 2:
			porcentaje = PORCENTAJE_REDUCIDO;
			break;
		case 3:
			porcentaje = PORCENTAJE_SUPERREDUCIDO;
			break;
		}
		
		
		Double iva = porcentaje * precioSinIva;
		Double precioConIva = precioSinIva + iva;
		System.out.println("El iva es " + iva);
		System.out.println("El precio total es " + precioConIva);
		
		scanner.close();
	}

}
