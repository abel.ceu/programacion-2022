package ejercicio07;

import java.util.Scanner;

public class Ejercicio07tipoA {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		Boolean repetir = true;
		
		while(repetir) {
			System.out.println("Dime un n�mero");
			Integer a = scanner.nextInt();
			System.out.println("Dime otro n�mero");
			Integer b = scanner.nextInt();
			if (a==b) {
				repetir = false;
			}
		}
		
		System.out.println("Gracias por todo!!");
		scanner.close();
		
		
	}
	
}
