package ejercicio07;

import java.util.Scanner;

public class Ejercicio07tipoB {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Integer a, b;
		
		do {
			System.out.println("Dime un n�mero");
			a = scanner.nextInt();
			System.out.println("Dime otro n�mero");
			b = scanner.nextInt();
		} while (a != b);

		System.out.println("Muchas gracias!!");

		scanner.close();

	}

}
