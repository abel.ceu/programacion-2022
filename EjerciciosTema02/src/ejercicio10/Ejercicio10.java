package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String cadena1;
		String cadena2;
		do {
			System.out.println("Dame una cadena");
			cadena1 = scanner.nextLine();
			System.out.println("Dame otra cadena");
			cadena2 = scanner.nextLine();
		}
		while(cadena1.isBlank() || cadena2.isBlank());
		
		if (cadena1.compareTo(cadena2) < 0) {
			System.out.println(cadena1);
			System.out.println(cadena2);
		}
		else {
			System.out.println(cadena2);
			System.out.println(cadena1);
			
		}
		
		scanner.close();
		
		
	}
	
	
}
