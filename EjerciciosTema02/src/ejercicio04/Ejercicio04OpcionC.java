package ejercicio04;

import java.util.Scanner;

public class Ejercicio04OpcionC {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Integer numero;
		Integer suma = 0;
		
		Boolean repetir = true;
		
		while(repetir) {
			System.out.println("Dame un n�mero");
			numero = scanner.nextInt();
			suma = suma + numero; // suma += numero;
			if (numero == 0) {
				repetir = false;
			}
			
		}
		
		System.out.println("Resultado = " + suma);
		
		scanner.close();
		
	}
}
