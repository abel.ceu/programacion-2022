package ejercicio09;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dame tu nombre completo");
		String nombreCompleto = scanner.nextLine();
		
		nombreCompleto = nombreCompleto.trim(); // opcional para evitar que el usuario nos ponga espacios adicionales
		
		Integer primerEspacio = nombreCompleto.indexOf(" ");
		Integer ultimoEspacio = nombreCompleto.lastIndexOf(" ");
		
		String nombre = nombreCompleto.substring(0, primerEspacio);
		String apellido1 = nombreCompleto.substring(primerEspacio+1, ultimoEspacio);
		String apellido2 = nombreCompleto.substring(ultimoEspacio+1);
		
		System.out.println("Nombre: " + nombre);
		System.out.println("Apellido 1: " + apellido1);
		System.out.println("Apellido 2: " + apellido2);
		
		
		scanner.close();
		
		
	}
	
	
}
