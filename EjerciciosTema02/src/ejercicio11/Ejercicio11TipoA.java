package ejercicio11;

import java.util.Scanner;

public class Ejercicio11TipoA {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Boolean emailCorrecto = false;
		do {
			System.out.println("Dime un email");
			String email = scanner.nextLine();
			Integer indiceArroba = email.indexOf("@");
			if (indiceArroba > 0) {
				String parteFinal = email.substring(indiceArroba+1);
				if (parteFinal.contains(".") 
						&& !parteFinal.endsWith(".")
						&& !parteFinal.startsWith(".")) {
					emailCorrecto = true;
				}
			}
		}
		while(!emailCorrecto);
		
		scanner.close();
		
	}
	
	
	
}
