package ejercicio11;

import java.util.Scanner;

public class Ejercicio11TipoB {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Integer indiceArroba;
		String email;
		String parteFinal;
		do {
			System.out.println("Dime un email");
			email = scanner.nextLine();
			indiceArroba = email.indexOf("@");
			parteFinal = email.substring(indiceArroba+1);
		}
		while(!parteFinal.contains(".") 
				|| parteFinal.endsWith(".") 
				|| parteFinal.startsWith(".") 
				|| indiceArroba < 0);
		
		scanner.close();
		
	}
	
	
	
}
