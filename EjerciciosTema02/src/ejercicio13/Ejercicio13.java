package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Dime un texto bonito");
		String texto = scanner.nextLine();

		Integer x = 0;
		Integer y = 5;
		while(y <= texto.length()) {
			System.out.println(texto.substring(x, y));
			x = x + 5;
			y = y + 5;
		}
		System.out.println(texto.substring(x));
		
		scanner.close();
	}

}
