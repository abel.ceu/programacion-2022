package ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dime un n�mero");
		Integer numero = scanner.nextInt();
		Integer contador = 0;
		
		while(contador <= 10) {
			System.out.println(numero + "x" + contador + " = " + (numero*contador));
			contador ++;
		}
		
		scanner.close();
		
	}
}
