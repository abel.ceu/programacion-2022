package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		String usuario;
		do {
			System.out.println("Dime el usuario");
			// usuario = scanner.nextLine().trim().toUpperCase();
			usuario = scanner.nextLine();
			usuario = usuario.trim();
			usuario = usuario.toUpperCase();
			
			if (usuario.length() < 10) {
				System.out.println("El tiene que tener longitud 10 o m�s");
			}
			if (usuario.contains(" ")) {
				System.out.println("El usuario no puede contener espacios");
			}

		} while (usuario.length() < 10 || usuario.contains(" "));

		System.out.println("Usuario correcto: " + usuario);
		
		scanner.close();
	}

}
