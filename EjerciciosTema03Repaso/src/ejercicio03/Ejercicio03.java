package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		Integer[] listaNotas  = new Integer[] {7, 9, 8, 4, 0, 6, 3, 4, 1, 7, 3, 1, 9, 10};
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("�Qu� nota buscas?");
		Integer notaBuscada = scanner.nextInt();
		
		Boolean encontrada = false;
		Integer posicionEncontrada = 0;
		for (int i = 0; i < listaNotas.length; i++) {
			if (listaNotas[i] == notaBuscada) {
				encontrada = true;
				posicionEncontrada = i;
				break;
			}
		}
		if (encontrada) {
			System.out.println("La nota buscada existe y est� en la posici�n " + posicionEncontrada);
		}
		else {
			System.out.println("La nota buscada no existe");
		}
		
		scanner.close();
		
	}

}
