package ejercicio02;

public class Ejercicio02 {

	public static void main(String[] args) {
		Integer[] listaNotas  = new Integer[] {7, 9, 8, 4, 0, 6, 3, 4, 1, 7, 3, 1, 9, 10};
		
		Integer sumaAprobados = 0;
		Integer cantAprobados = 0;
		for (int i = 0; i < listaNotas.length; i++) {
			if (listaNotas[i] >= 5) {
				sumaAprobados+=listaNotas[i];
				cantAprobados++;
			}
		}
		Integer mediaAprobados = sumaAprobados/cantAprobados;
		System.out.println("Nota media de aprobados: " + mediaAprobados);
		
		
	}

}
