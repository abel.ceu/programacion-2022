package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		Integer[] listaNotas  = new Integer[] {7, 9, 8, 4, 0, 6, 3, 4, 1, 7, 3, 1, 9, 10};
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("�Qu� nota buscas?");
		Integer notaBuscada = scanner.nextInt();
		
		Integer cantidadEncontrada = 0;
		for (int i = 0; i < listaNotas.length; i++) {
			if (listaNotas[i] > notaBuscada) {
				cantidadEncontrada++;
			}
		}
		System.out.println("Hay " + cantidadEncontrada + " notas mayores a la indicada");
		
		scanner.close();
		
	}


}
