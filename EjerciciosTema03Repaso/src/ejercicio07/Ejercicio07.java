package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {

	private static final double PRECIO_PALABRA = 0.24;
	private static final String STOP = "stop";

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Escribe tu telegrama");
		String telegrama = scanner.nextLine();
		
		String[] palabras = telegrama.split(" ");
		Integer cantidad = 0;
		for (String palabra : palabras) {
			if (!palabra.equalsIgnoreCase(STOP)) {
				cantidad++;
			}
		}
		
		Double precio = cantidad * PRECIO_PALABRA;
		System.out.println("El precio es " + precio);
		
		scanner.close();
	}
}
