package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dime una palabra");
		String palabra = scanner.nextLine();
		for (int i = palabra.length(); i > 0; i--) {
			System.out.print(palabra.substring(i-1, i));
		}
		
		scanner.close();
	}
}
