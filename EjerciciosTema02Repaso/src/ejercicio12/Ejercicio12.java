package ejercicio12;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dame un n�mero mayor a cero");
		Integer tama�o = scanner.nextInt();

		// Imprimiendo la tapadera
		System.out.print(" __");
		for (int i = 1; i < tama�o; i++) {
			System.out.print("___");
		}
		System.out.println();
		
		// Imprimiendo la cuadr�cula
		for (int j = 0; j < tama�o; j++) {
			
			for (int i = 0; i < tama�o; i++) {
				System.out.print("|__");
			}
			System.out.println("|");

		}
		
		scanner.close();

	}
}
