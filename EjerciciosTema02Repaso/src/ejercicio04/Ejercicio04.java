package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dime el precio");
		Double precio = scanner.nextDouble();
		System.out.println("Dime unidades");
		Integer unidades = scanner.nextInt();

		Double porcentajeDto = 0.0;
		if (unidades > 100) {
			porcentajeDto = 0.4;
		}
		else if (unidades >= 25) {
			porcentajeDto = 0.2;
		}
		else if (unidades >= 10) {
			porcentajeDto = 0.1;
		}
		Double subtotal = unidades * precio;
		Double descuento = subtotal * porcentajeDto;
		Double total = subtotal - descuento;
		System.out.println("Subtotal: " + subtotal);
		System.out.println("Descuento: " + descuento);
		System.out.println("Total: " + total);
		scanner.close();
	}
}
