package ejercicio21;

import java.util.Scanner;

public class Ejercicio21 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Qu� dimensi�n de tablero quieres?");
		Integer dimension = scanner.nextInt();

		for (int j = 0; j < dimension; j++) {

			for (int i = 0; i < dimension; i++) {
				if (j == i) {
					System.out.print(" * ");
				} else {
					System.out.print(" - ");
				}
			}
			System.out.println();

		}

		scanner.close();
	}

}
