package ejercicio17;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		Integer numero;
		
		do {
			System.out.println("Dime un n�mero mayor a 2");
			numero = scanner.nextInt();
		}
		while(numero <= 2);
		
		Integer x = 0;
		Integer y = 1;
		System.out.print("0, 1");
		for(int i=3; i<=numero; i++) {
			Integer siguiente = x + y;
			System.out.print(", " + siguiente);
			x = y;
			y = siguiente;
		}
		scanner.close();
		
		
	}

}
