package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dime tres ciudades");
		String c1 = scanner.nextLine();
		String c2 = scanner.nextLine();
		String c3 = scanner.nextLine();
		Integer long1 = c1.length();
		Integer long2 = c2.length();
		Integer long3 = c3.length();
		
		if (long1 < long2 && long1 < long3) {
			System.out.println(c1);
			if (long2<long3) {
				System.out.println(c2);
				System.out.println(c3);
			}
			else {
				System.out.println(c3);
				System.out.println(c2);
			}
		}
		else if (long2 < long1 && long2 < long3) {
			System.out.println(c2);
			if (long1<long3) {
				System.out.println(c1);
				System.out.println(c3);
			}
			else {
				System.out.println(c3);
				System.out.println(c1);
			}
		}
		else {
			System.out.println(c3);
			if (long2<long1) {
				System.out.println(c2);
				System.out.println(c1);
			}
			else {
				System.out.println(c1);
				System.out.println(c2);
			}
		}
		
		
		scanner.close();
	
	}
}
