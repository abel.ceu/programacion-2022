package ejercicio09;

import java.util.Scanner;


public class Ejercicio09 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Boolean empiezaOK;
		Boolean terminaOK;
		Boolean terminaOKFinal;
		do {
			System.out.println("Cu�ntame cositas...");
			String texto = scanner.nextLine();
			empiezaOK = texto.startsWith("hola");
			terminaOK = texto.endsWith("hastaluego");
			terminaOKFinal = texto.endsWith("adios");
			if (empiezaOK && terminaOK) {
				String msg = texto.substring(4, texto.lastIndexOf("hastaluego"));
				System.out.println("Te he entendido: " + msg.trim());
			}
			else if (empiezaOK && terminaOKFinal) {
				String msg = texto.substring(4, texto.lastIndexOf("adios"));
				System.out.println("Te he entendido: " + msg.trim());
			}
			else {
				System.out.println("No te entiendo. Repite");
			}
		}
		while(!(empiezaOK && terminaOKFinal));
		scanner.close();
		
		
		
	}
	
	
}
