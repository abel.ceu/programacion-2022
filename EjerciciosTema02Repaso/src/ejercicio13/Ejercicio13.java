package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String frase;
		String acumulado = "";
		do {
			System.out.println("Dame unos versos bonitos");
			frase = scanner.nextLine();
			if (!frase.equalsIgnoreCase("fin")) {
				acumulado += frase + " ";
			}
			
		} while (!frase.equalsIgnoreCase("fin"));
		
		System.out.println("Este es tu poema: ");
		System.out.println(acumulado);
		scanner.close();
	}
	
	
}
