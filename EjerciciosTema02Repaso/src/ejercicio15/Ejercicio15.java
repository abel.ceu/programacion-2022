package ejercicio15;

import java.util.Random;
import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		// generamos aleatorio del 0 al 9
		Random random = new Random();
		Integer aleatorio = random.nextInt(10);
		Integer apuesta;
		Integer contador = 0;
		do {
			System.out.println("Averigua mi numerito");
			apuesta = scanner.nextInt();
			if (aleatorio != apuesta) {
				System.out.println("No lo has acertado");
			}
			contador ++;
		}
		while(aleatorio != apuesta && contador < 10);
		
		Integer puntuacion = 10 - contador;
		System.out.println("Tu puntuación es: " + puntuacion);
		
		scanner.close();
		
	}

}
