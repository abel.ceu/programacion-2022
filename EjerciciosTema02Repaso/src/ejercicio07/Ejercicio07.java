package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		Integer numero;
		do {
			System.out.println("Dime un n�mero");
			numero = scanner.nextInt();
			Integer numeroX10 = numero * 10;
			if (numero <= 0 || numero > 10) {
				Integer suma = 0;
				for (int i = numero; i < numeroX10; i+=numero) {
					System.out.print(i + " + ");
					suma += i;
				}
				suma += numeroX10;
				System.out.println(numeroX10 +" = " + suma);
			}
			else if (numero != 0){
				System.out.println("Error. El n�mero no es v�lido");
			}
		}
		while(numero != 0);
			
		scanner.close();
		
		
		
	}
}
