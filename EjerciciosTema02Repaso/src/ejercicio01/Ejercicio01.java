package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {
	
	public static final Integer A�O_REFERENCIA = 2030;
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dime tu nombre");
		String nombre = scanner.nextLine();
		System.out.println("Dime tu a�o de nacimiento");
		Integer a�oNacimiento = scanner.nextInt();
		
		Integer edadFutura = A�O_REFERENCIA - a�oNacimiento;
		
		System.out.println("Hola " + nombre + " tu edad en el 2030 ser� " + edadFutura);
		
		
		scanner.close();
	}

}
