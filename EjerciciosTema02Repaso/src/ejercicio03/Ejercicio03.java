package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		Scanner scanner  = new Scanner(System.in);
		Integer opcion;
		do {
			System.out.println("Elija una opci�n:");
			System.out.println("\t1. Abrir");
			System.out.println("\t2. Guardar");
			System.out.println("\t3. Modificar");
			System.out.println("\t4. Salir");
			opcion = scanner.nextInt();
			switch (opcion) {
			case 1:
				System.out.println("Has elegido abrir");
				break;
			case 2:
				System.out.println("Has elegido guardar");
				break;
			case 3:
				System.out.println("Has elegido modificar");
				break;
			case 4:
				System.out.println("BYE BYE");
				break;
			default:
				System.out.println("Error en la opci�n elegida");
				break;
			}
		}
		while(opcion != 4);
		
		scanner.close();
	}
	
}
