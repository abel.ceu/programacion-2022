package ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dame un n�mero");
		Integer x = scanner.nextInt();
		System.out.println("Dame otro n�mero");
		Integer y = scanner.nextInt();
		Integer opcion;
		do {
			System.out.println("*** MEN� ***\n\t1. Sumar\n\t2. Restar\n\t3. Multiplicar\n\t4. Dividir\n\t0. Salir");
			opcion = scanner.nextInt();
			switch (opcion) {
			case 0:
				System.out.println("Bye Bye");
				break;
			case 1:
				System.out.println("Resultado: " + (x+y));
				break;
			case 2:
				System.out.println("Resultado: " + (x-y));
				break;
			case 3:
				System.out.println("Resultado: " + (x*y));
				break;
			case 4:
				if (y != 0) {
					System.out.println("Resultado: " + (x/y));
				}
				else {
					System.out.println("No podemos dividir por cero");
				}
					break;
			default:
				System.out.println("Opci�n incorrecta");
				break;
			}
			
		}
		while(opcion != 0);
		
		scanner.close();
		
		
		
		
	}
}
