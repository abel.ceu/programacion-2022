package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Boolean empiezaOK;
		Boolean terminaOK;
		do {
			System.out.println("Cu�ntame cositas...");
			String texto = scanner.nextLine();
			empiezaOK = texto.startsWith("hola");
			terminaOK = texto.endsWith("hastaluego");
			if (empiezaOK && terminaOK) {
				String msg = texto.substring(4, texto.lastIndexOf("hastaluego"));
				System.out.println("Te he entendido: " + msg.trim());
				break;
			}
			else {
				System.out.println("No te entiendo. Repite");
			}
		}
		while(true);
		
		scanner.close();
		
		
		
	}
	
	
}
