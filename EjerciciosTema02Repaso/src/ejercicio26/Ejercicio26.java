package ejercicio26;

import java.util.Random;
import java.util.Scanner;

public class Ejercicio26 {

	private static final String PIEDRA 	= "PIEDRA";
	private static final String PAPEL 	= "PAPEL";
	private static final String TIJERAS = "TIJERAS";

	private static final Integer OPCION_PIEDRA 	= 0;
	private static final Integer OPCION_PAPEL 	= 1;
	private static final Integer OPCION_TIJERAS = 2;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Random random = new Random();
		String[] opciones = new String[3];
		
		opciones[OPCION_PIEDRA] = PIEDRA;
		opciones[OPCION_PAPEL] = PAPEL;
		opciones[OPCION_TIJERAS] = TIJERAS;
		
		Integer victorias = 0, empates = 0, derrotas = 0;

		String opcionRepetir;
		do {

			// 1. Recogemos la opci�n escogida por el usuario validando que sea correcta
			String eleccionUsuario;
			Integer opcionUsuario = -1;
			do {
				System.out.println("Piedra, papel o tijeras... un, dos, tres, ya!! ");
				eleccionUsuario = scanner.nextLine().trim().toUpperCase();
				switch (eleccionUsuario) {
				case PIEDRA:
					opcionUsuario = OPCION_PIEDRA;
					break;
				case PAPEL:
					opcionUsuario = OPCION_PAPEL;
					break;
				case TIJERAS:
					opcionUsuario = OPCION_TIJERAS;
					break;
				default:
					System.out.println("Esa opci�n no es v�lida. Tienes que indicar piedra, papel o tijeras. Vuelve a intentarlo");
					break;
				}
			} while (opcionUsuario < 0);

			
			// 2. Obtenemos la opci�n aleatorio jugada por la m�quina
			Integer opcionMaquina = random.nextInt(3);
			System.out.println("Yo he sacado: " + opciones[opcionMaquina]);
			
			// 3. Verificamos si ha habido empate
			if (opcionMaquina == opcionUsuario) {
				System.out.println("EMPATE!! ");
				empates++;
			}
			// 4. Verificamos resto de opciones
			else {
				if (opcionUsuario != OPCION_PAPEL && opcionMaquina != OPCION_PAPEL) {
					opcionUsuario = opcionUsuario*-1;
					opcionMaquina = opcionMaquina*-1;
				}
				if (opcionUsuario > opcionMaquina) {
					System.out.println("Has ganado!! ");
					victorias++;
				}
				else {
					System.out.println("Has perdido!! ");
					derrotas++;
				}
			}
			
			
			// 5. Preguntamos si quiere jugar de nuevo
			Boolean respuestaCorrecta;
			do {
				System.out.println("�Quieres jugar de nuevo? (S/N)");
				opcionRepetir = scanner.nextLine().trim();
				if (!opcionRepetir.equalsIgnoreCase("S") && !opcionRepetir.equalsIgnoreCase("N")) {
					System.out.println("No te he entendido.");
					respuestaCorrecta = false;
				} 
				else {
					respuestaCorrecta = true;
				}
			} while (!respuestaCorrecta);

		} while (opcionRepetir.equalsIgnoreCase("S"));
		
		// 6. Mostramos marcador
		System.out.println("N�mero de victorias: " + victorias);
		System.out.println("N�mero de derrotas: " + derrotas);
		System.out.println("N�mero de empates: " + empates);


		scanner.close();

	}
	
}
