package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dame una URL");
		String url = scanner.nextLine();
		
		Integer indiceBarra = url.indexOf("//");
		Integer indicePunto1 = url.indexOf(".");
		Integer indicePunto2 = url.lastIndexOf(".");
		
		if (indiceBarra == -1 || indicePunto1 == -1 || indicePunto1 == indicePunto2) {
			System.out.println("El formato de la URL no es correcta");
		}
		else {
			String protocolo = url.substring(0, indiceBarra + 2);
			String parte1 = url.substring(indiceBarra+2, indicePunto1); 
			String parte2 = url.substring(indicePunto1+1, indicePunto2);
			String parte3 = url.substring(indicePunto2+1);
			
			System.out.println(protocolo);
			System.out.println(parte1);
			System.out.println(parte2);
			System.out.println(parte3);
		}
		
		scanner.close();
		
		
		
			
		
		
		
	}
	
	
}
