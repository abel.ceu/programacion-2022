package ejercicio24;

import java.util.Scanner;

public class Ejercicio24 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		// Pedimos un n�mero mayor a 0
		Integer numero;
		do {
			System.out.println("Dime un n�mero mayor a 0");
			numero = scanner.nextInt();
		}
		while(numero <= 0);
		
		// Sumamos todos los n�meros desde el 1 hasta el indicado
		Integer suma = 0;
		for (int i = 1; i <= numero; i++) {
			if (i%2 == 1) {
				suma = suma + i;
			}
		}
		System.out.println(suma);
		
		scanner.close();
		
	}

}
