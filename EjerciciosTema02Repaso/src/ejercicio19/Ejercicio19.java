package ejercicio19;

import java.util.Scanner;

public class Ejercicio19 {

	private static final double FACTOR_CONVERSION = 166.386;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Integer opcion;
		do {
			System.out.println("\t1 - Pasar de Pesetas a Euros\r\n"
					+ "\t2 - Pasar de Euros a Pesetas\r\n"
					+ "\t3 - Salir");
			
			System.out.println("Qu� quieres??");
			opcion = scanner.nextInt();
			
			if (opcion < 1 || opcion > 3) {
				System.out.println("La opci�n elegida no es correcta");
			}
			else {
				System.out.println("Dime la cantidad");
				Double cantidad = scanner.nextDouble();
				if(opcion == 1) {
					System.out.println("Resultado: " + (cantidad/FACTOR_CONVERSION));
				}
				else {
					System.out.println("Resultado: " + (cantidad*FACTOR_CONVERSION));
				}
				
			}
			
		} 
		while(opcion != 3);
		System.out.println("Bye Bye!!");
		scanner.close();
		
	}

}
