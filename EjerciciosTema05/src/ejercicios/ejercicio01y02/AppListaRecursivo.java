package ejercicios.ejercicio01y02;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class AppListaRecursivo {
	public static void main(String[] args) {
		Scanner sc = null;
		try {
			sc = new Scanner(System.in);
			List<Integer> numeros = new ArrayList<>();
			leerNumeros(sc, numeros);
		}
		finally {
			if (sc != null) {
				sc.close();
			}
		}
		
	}

	private static void leerNumeros(Scanner sc, List<Integer> numeros) {
		Integer num = 0;
		try {
			System.out.println("Dame un n�mero");
			num = sc.nextInt();
			if (num != -1) {
				numeros.add(num);
				leerNumeros(sc, numeros);
			}
		}
		catch(InputMismatchException e) {
			System.out.println("No puedes introducir palabras");
			sc.nextLine();
			leerNumeros(sc, numeros);
		}
		
	}
}
