package ejercicios.ejercicio01y02;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class AppLista {
	public static void main(String[] args) {
		Scanner sc = null;
		try {
			sc = new Scanner(System.in);
			List<Integer> numeros = new ArrayList<>();
			Integer num = 0;
			do {
				try {
					System.out.println("Dame un n�mero");
					num = sc.nextInt();
					if (num != -1) {
						numeros.add(num);
					}
				}
				catch(InputMismatchException e) {
					System.out.println("No puedes introducir palabras");
					sc.nextLine();
				}
			} while (num != -1);
		}
		finally {
			if (sc != null) {
				sc.close();
			}
		}
		
	}
}
