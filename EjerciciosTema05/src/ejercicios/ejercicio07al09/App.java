package ejercicios.ejercicio07al09;

import java.math.BigDecimal;
import java.util.Scanner;

import ejercicios.ejercicio06.ParametroIncorrectoException;

public class App {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Sociedad sociedad = new Sociedad();
		String terminar;
		do {
			System.out.println("Indica un g�nero:");
			String genero = sc.nextLine();
			System.out.println("Indica una altura:");
			BigDecimal altura = sc.nextBigDecimal();
			sc.nextLine();
			try {
				sociedad.addPersona(genero, altura);
			} catch (ParametroIncorrectoException e) {
				System.out.println("Alguno de los parametros no es correcto. Int�ntalo de nuevo.");
			}
			System.out.println("Terminar?? (S/N)");
			terminar = sc.nextLine();
		}
		while(!terminar.equalsIgnoreCase("S"));
		
		try {
			System.out.println("Mujeres: " + sociedad.calcularAlturaMediaMujeres());
		} catch (ListaVaciaException e1) {
			System.out.println("No hay mujeres");
		}
		
		try {
			System.out.println("Hombres: " + sociedad.calcularAlturaMediaHombres());
		} catch (ListaVaciaException e) {
			System.out.println("No hay hombres");
		}
		try {
			System.out.println("Todos: " + sociedad.calcularAlturaMedia());
		} catch (ListaVaciaException e) {
			System.out.println("No hay personas");
		}
		
		sc.close();
		
		
	}

}
