package ejercicios.ejercicio07al09;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import ejercicios.ejercicio06.ParametroIncorrectoException;
import ejercicios.ejercicio06.Persona;

public class Sociedad {

	private Set<Persona> personas;

	public Sociedad() {
		personas = new HashSet<>();
	}

	public void addPersona(String genero, BigDecimal altura) throws ParametroIncorrectoException {
		Persona p = new Persona();
		try {
			p.setGenero(genero);
			p.setAltura(altura);
			personas.add(p);
		} catch (ParametroIncorrectoException e) {
			System.out.println(e.getMessage());
			throw e;
		}
	}

//	public BigDecimal calcularAlturaMedia() throws ListaVaciaException {
//		if (personas.isEmpty()) {
//			throw new ListaVaciaException("No hay personas");
//		}
//		BigDecimal total = BigDecimal.ZERO;
//		for (Persona persona : personas) {
//			total = total.add(persona.getAltura());
//		}
//		return total.divide(new BigDecimal(personas.size()), 2, RoundingMode.HALF_DOWN);
//	}
	
	private BigDecimal calcularAlturaMedia(String genero) throws ListaVaciaException {
		Integer cantidad = 0;
		BigDecimal total = BigDecimal.ZERO;
		for (Persona persona : personas) {
			if (persona.getGenero().equalsIgnoreCase(genero) 
					|| genero.equals("Todos")) {
				total = total.add(persona.getAltura());
				cantidad++;
			}
		}
		if (cantidad == 0) {
			throw new ListaVaciaException("No hay personas");
		}
		return total.divide(new BigDecimal(cantidad), 2, RoundingMode.HALF_DOWN);
	}
	
	public BigDecimal calcularAlturaMedia() throws ListaVaciaException {
		return calcularAlturaMedia("Todos");
	}
	
	public BigDecimal calcularAlturaMediaHombres() throws ListaVaciaException {
		return calcularAlturaMedia("H");
	}
	
	public BigDecimal calcularAlturaMediaMujeres() throws ListaVaciaException {
		return calcularAlturaMedia("M");
	}
	
	public void borrarGenero(String genero) throws ParametroIncorrectoException {
		if (!genero.equalsIgnoreCase("H") && !genero.equalsIgnoreCase("M")) {
			throw new ParametroIncorrectoException("El g�nero indicado no es correcto");
		}
		Iterator<Persona> it = personas.iterator();
		while (it.hasNext()) {
			Persona persona = (Persona) it.next();
			if (persona.getGenero().equalsIgnoreCase(genero)) {
				it.remove();
			}
		}
	}
	

}





