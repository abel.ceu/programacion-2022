package ejercicios.ejercicio03al05;

import java.util.InputMismatchException;
import java.util.Scanner;

public class App {
	public static void main(String[] args) {
		Scanner sc = null;
		try {
			sc = new Scanner(System.in);
			SacoNumeros saco = new SacoNumeros();
			
			Integer num = 0;
			do {
				try {
					System.out.println("Dame un n�mero");
					num = sc.nextInt();
					if (num != -1) {
						saco.addNumero(num);
					}
				}
				catch(InputMismatchException e) {
					System.out.println("No puedes introducir palabras");
					sc.nextLine();
				}
			} while (num != -1);
			
			do {
				try {
					System.out.println("Dame una posici�n");
					num = sc.nextInt();
					System.out.println(saco.getNumero(num));
				}
				catch(InputMismatchException e) {
					System.out.println("No puedes introducir palabras");
					sc.nextLine();
				}
			} while (num != -1);
		}
		finally {
			if (sc != null) {
				sc.close();
			}
		}
		
		
		
		
		
		
		
	}
}
