package ejercicios.ejercicio03al05;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class SacoNumeros {
	private List<Integer> lista;
	
	public SacoNumeros() {
		lista = new ArrayList<>();
	}
	
	public void addNumero(Integer numero) {
		lista.add(numero);
	}
	
	public Integer getNumero(int posicion) {
		try {
			return lista.get(posicion);
		}
		catch(IndexOutOfBoundsException e) {
			return null;
		}
	}
	
	public BigDecimal dividir() {
		try {
			BigDecimal primero = new BigDecimal(lista.get(0));
			BigDecimal segundo = new BigDecimal(lista.get(1));
			BigDecimal resultado = primero.divide(segundo, 2, RoundingMode.HALF_DOWN);
			for (int i = 2; i < lista.size(); i++) {
				BigDecimal divisor = new BigDecimal(lista.get(i));
				resultado = resultado.divide(divisor, 2, RoundingMode.HALF_DOWN);
			}
			return resultado;
		}
		catch(ArithmeticException | IndexOutOfBoundsException e) {
			return BigDecimal.ZERO.setScale(2);
		}
		
		
	
	}
	
	
	
	@Override
	public String toString() {
		return "Saco: " + lista;
	}
}
