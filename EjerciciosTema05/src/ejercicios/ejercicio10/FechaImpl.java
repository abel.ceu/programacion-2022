package ejercicios.ejercicio10;

import java.time.DateTimeException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

public class FechaImpl implements Fecha{

	private LocalDate fecha;
	private String formato;
	
	public FechaImpl() {
		fecha = LocalDate.now();
		formato = "dd/MM/yyyy";
	}
	
	public FechaImpl(Integer dia, Integer mes, Integer a�o) throws FechaIncorrectaException {
		try {
			formato = "dd/MM/yyyy";
			fecha = LocalDate.of(a�o, mes, dia);
		}
		catch(DateTimeException e) {
			throw new FechaIncorrectaException("Fecha incorrecta", e);
		}
	}
	
	
	@Override
	public Boolean validar() {
		// ya estamos validando que la fecha siempre sea correcta cuando se crea o se cambia
		// por tanto, siempre ser� v�lida
		return true; 
	}

	@Override
	public Integer getDia() {
		return fecha.getDayOfMonth();
	}

	@Override
	public Integer getMes() {
		return fecha.getMonthValue();
	}

	@Override
	public Integer getA�o() {
		return fecha.getYear();
	}

	@Override
	public Integer getGmt() {
		throw new RuntimeException("No implementado");
	}

	@Override
	public String getFormato() {
		return formato;
	}

	@Override
	public void setDia(Integer dia) throws FechaIncorrectaException {
		try {
			fecha = fecha.withDayOfMonth(dia);
		}
		catch(DateTimeException e) {
			throw new FechaIncorrectaException("El d�a indicado no es una fecha v�lida", e);
		}
	}

	@Override
	public void setMes(Integer mes) throws FechaIncorrectaException {
		try {
			fecha = fecha.withMonth(mes);
		}
		catch(DateTimeException e) {
			throw new FechaIncorrectaException("El mes indicado no es una fecha v�lida", e);
		}
	}

	@Override
	public void setA�o(Integer a�o) throws FechaIncorrectaException {
		try {
			fecha = fecha.withYear(a�o);
		}
		catch(DateTimeException e) {
			throw new FechaIncorrectaException("El a�o indicado no es una fecha v�lida", e);
		}
	}

	@Override
	public void setGmt(Integer gmt) {
		throw new RuntimeException("No implementado");
	}

	@Override
	public void setFormato(String formato) {
		this.formato = formato;
	}

	@Override
	public Boolean isMayor(Fecha fecha) {
		LocalDate date = LocalDate.of(fecha.getA�o(), fecha.getMes(), fecha.getDia());
		return this.fecha.isAfter(date);
	}

	@Override
	public Boolean isMenor(Fecha fecha) {
		LocalDate date = LocalDate.of(fecha.getA�o(), fecha.getMes(), fecha.getDia());
		return this.fecha.isBefore(date);
	}

	@Override 
	public Integer restarFecha(Fecha fecha) {
		LocalDate date = LocalDate.of(fecha.getA�o(), fecha.getMes(), fecha.getDia());
		return Long.valueOf(ChronoUnit.DAYS.between(date, this.fecha)).intValue();
	}

	@Override
	public Boolean isBisiesto() {
		return fecha.isLeapYear();
	}

	@Override
	public Boolean isFestivo() {
		// s�lo podemos ver los festivos que son fijos en espa�a y los domingos
		return (getDia().equals(25) && getMes().equals(12)) // navidad
			|| (getDia().equals(1) && getMes().equals(1)) // a�o nuevo
			|| (getDia().equals(1) && getMes().equals(11)) // todos los santos
			|| (getDia().equals(12) && getMes().equals(10)) // hispanidad
			|| (getDia().equals(6) && getMes().equals(1)) // reyes
			|| (getDia().equals(6) && getMes().equals(12)) // constituci�n
			|| (getDia().equals(8) && getMes().equals(12)) // inmaculada
			|| fecha.getDayOfWeek() == DayOfWeek.SUNDAY; // domingos
	}

	@Override
	public void sumarDias(Integer dias) {
		fecha = fecha.plusDays(dias);
		
	}

	@Override
	public Integer getNumSemana() {
		return fecha.get(ChronoField.ALIGNED_WEEK_OF_YEAR);
	}
	
	@Override
	public String toString() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
		return fecha.format(formatter);
	}

	@Override
	public int hashCode() {
		return Objects.hash(fecha);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FechaImpl other = (FechaImpl) obj;
		return Objects.equals(fecha, other.fecha);
	}
	

	
	
}
