package ejercicios.ejercicio10;

public class App {

	public static void main(String[] args) {

		try {
			new FechaImpl(29,2,2023);
		} catch (FechaIncorrectaException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			FechaImpl fecha = new FechaImpl(10,4,2023);
			fecha.setFormato("dd-MM-yyyy");
			fecha.sumarDias(6);
			System.out.println("Fecha: " + fecha);
			System.out.println("D�as desde a�o nuevo: " + fecha.restarFecha(new FechaImpl(1, 1, 2023)));
			System.out.println("Es festivo: " + fecha.isFestivo());
			System.out.println("Es bisiesto: " + fecha.isBisiesto());
			System.out.println("Semana del a�o: " + fecha.getNumSemana());
			
		} catch (FechaIncorrectaException e) {
			System.out.println(e.getMessage());
		}
		
	}
}
