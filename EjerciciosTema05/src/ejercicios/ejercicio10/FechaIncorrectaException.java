package ejercicios.ejercicio10;

public class FechaIncorrectaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4500512056161265050L;

	public FechaIncorrectaException() {
	}

	public FechaIncorrectaException(String message) {
		super(message);
	}

	public FechaIncorrectaException(Throwable cause) {
		super(cause);
	}

	public FechaIncorrectaException(String message, Throwable cause) {
		super(message, cause);
	}

	public FechaIncorrectaException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
