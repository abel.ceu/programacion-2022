package ejercicios.ejercicio06;

import java.math.BigDecimal;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
	
		Persona persona = leerPersona();
		System.out.println(persona);
	}
	
	public static Persona leerPersona() {
		Scanner sc = new Scanner(System.in);
		System.out.println("D�game el g�nero");
		String genero = sc.nextLine();
		System.out.println("D�game la altura");
		BigDecimal altura = sc.nextBigDecimal();
		Persona p = new Persona();
		try {
			p.setGenero(genero);
			p.setAltura(altura);
		}
		catch(ParametroIncorrectoException e) {
			System.out.println(e.getMessage());
		}
		sc.close();
		return p;
	}

}
