package ejercicios.ejercicio06;

import java.math.BigDecimal;

public class Persona {

	private String genero;
	private BigDecimal altura;
	
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) throws ParametroIncorrectoException {
//		if (genero.equalsIgnoreCase("H") || genero.equalsIgnoreCase("M")) {
//			// este ser�a el caso de g�nero correcto
//		}
		if (!genero.equalsIgnoreCase("H") && !genero.equalsIgnoreCase("M") ) {
			throw new ParametroIncorrectoException("El g�nero no es H ni M");
		}
		this.genero = genero;
	}
	public BigDecimal getAltura() {
		return altura;
	}
	public void setAltura(BigDecimal altura) throws ParametroIncorrectoException {
		if (altura.compareTo(BigDecimal.ZERO) <= 0 
				|| altura.compareTo(new BigDecimal(3)) >= 0) {
			throw new ParametroIncorrectoException("La altura no est� entre 0 y 3");
		}
		this.altura = altura;
	}
	
	@Override
	public String toString() {
		return "Persona [genero=" + genero + ", altura=" + altura + "]";
	}
	
	
}
