package repaso.ejercicio01;

import java.util.Scanner;

public class Ejercicio02Repaso {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dame un n�mero grande");
		Long numero1 = scanner.nextLong();
		System.out.println("Dame otro n�mero grande");
		Long numero2 = scanner.nextLong();
		Long multiplicacion = numero1*numero2;
		System.out.println("Resultado = " + multiplicacion);
		scanner.close();
	}

}
