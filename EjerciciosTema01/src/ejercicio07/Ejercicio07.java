package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dime tu altura");
		Double altura = scanner.nextDouble();
		System.out.println("Dime tu peso");
		Double peso = scanner.nextDouble();
		
		Double imc = peso / (altura*altura);
		System.out.println("Tu indice de masa corporal es " + imc);
		
		scanner.close();
	}
}
