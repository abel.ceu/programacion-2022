package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		// 1. Escribir mensaje para pedir nombre
		System.out.println("Dime tu nombre");
		
		// 2. Leer el nombre y guardarlo en variable
		Scanner scanner = new Scanner(System.in);
		String nombre = scanner.nextLine();
		
		// 3. Escribir mensaje para pedir apellido
		System.out.println("Dime tu apellido");
		
		// 4. Leer el apellido y guardarlo en variable
		String apellido = scanner.nextLine();
		
		// 5. Escribir mensaje con nombre y apellido todo junto 
		System.out.println("Tu nombre completo es " + nombre + " " + apellido);
	
		// cerrar scanner
		scanner.close();
	}

}
