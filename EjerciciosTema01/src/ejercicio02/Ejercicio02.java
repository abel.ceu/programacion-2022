package ejercicio02;

import java.util.Scanner;

public class Ejercicio02 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Indique su nombre de usuario");
		String nombreUsuario = scanner.nextLine();
		System.out.println("Bienvenido " + nombreUsuario);
		
		System.out.println("�De d�nde eres?");
		String lugar = scanner.nextLine();
		System.out.println("�Qu� tal se vive en " + lugar + "?");
		
		scanner.nextLine();
		System.out.println("Gracias. Un saludo");
		
		scanner.close();
	}
}
