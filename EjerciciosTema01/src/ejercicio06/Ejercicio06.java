package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {
	
	
	private static final double PORCENTAJE_IVA = 0.21;

	public static void main(String[] args) {
		System.out.println("�Cu�nto cuesta el art�culo sin IVA?");
		
		Scanner scanner = new Scanner(System.in);
		Double precioSinIVA = scanner.nextDouble();
	
		Double iva = precioSinIVA * PORCENTAJE_IVA;
		Double precioConIVA = precioSinIVA + iva;
	
		System.out.println("El IVA del art�culo es " + iva + " %");
		System.out.println("El precio con IVA es " + precioConIVA);
		
		scanner.close();
	}
}
