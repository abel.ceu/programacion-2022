package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {
	public static void main(String[] args) {
		
		System.out.println("�Cu�nto te gustar�a ganar al a�o?");
		
		Scanner scanner = new Scanner(System.in);
		Integer sueldoAnual = scanner.nextInt();
	
		Integer sueldoMensual = sueldoAnual / 12;
		System.out.println("Tu sueldo mensual ser� de " + sueldoMensual + " euros");
	
		scanner.close();
	}
}
