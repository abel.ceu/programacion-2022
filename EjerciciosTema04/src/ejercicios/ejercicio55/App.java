package ejercicios.ejercicio55;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class App {
	public static void main(String[] args) {
		DecimalFormat ft = new DecimalFormat("#,###.0");
		Evaluacion e = new Evaluacion();
		e.addNota("234234D", new BigDecimal(4));
		e.addNota("643543D", new BigDecimal(9.4));
		e.addNota("234244X", new BigDecimal(3.2));
		e.addNota("948383R", new BigDecimal(8.5));
		e.addNota("320093F", new BigDecimal(7.2));
		System.out.println(e);
		e.addNota("948383R", new BigDecimal(8.5));
		System.out.println(e);
		e.corregirNota("948383R", new BigDecimal(1));
		e.corregirNota("948383F", new BigDecimal(8.5));
		System.out.println(e);
		System.out.println("Nota alumno 320093F: " + ft.format(e.obtenerNotaAlumno("320093F")));
		System.out.println("Media: " + ft.format(e.obtenerNotaMedia()));
		System.out.println("Cant Aprobados: " +e.obtenerCantidadAprobados());
		System.out.println("Lista suspensos " + e.obtenerSuspensos());
		e.borrarAprobados();
		System.out.println(e);
	}
}
