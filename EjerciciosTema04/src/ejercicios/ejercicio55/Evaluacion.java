package ejercicios.ejercicio55;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Evaluacion {
	private static final BigDecimal APROBADO = new BigDecimal(5);
	
	private Map<String, BigDecimal> notas;
	
	public Evaluacion() {
		notas = new HashMap<>();
	}
	
	public Boolean addNota(String dni, BigDecimal nota) {
		if (notas.containsKey(dni)) {
			return false;
		}
		notas.put(dni, nota);
		return true;
	}
	public Boolean corregirNota(String dni, BigDecimal nota) {
		if (!notas.containsKey(dni)) {
			return false;
		}
		notas.put(dni, nota);
		return true;
	}
	public BigDecimal obtenerNotaAlumno(String dni) {
		return notas.getOrDefault(dni, BigDecimal.ZERO);
//		if (notas.containsKey(dni)) { // alternativa con IF
//			return notas.get(dni);
//		}
//		return BigDecimal.ZERO;
		
		// alternativa con IF corto
// 		return notas.containsKey(dni) ? notas.get(dni) : BigDecimal.ZERO;
	}
	
	public BigDecimal obtenerNotaMedia() {
		BigDecimal suma = BigDecimal.ZERO;
		for (BigDecimal nota : notas.values()) {
			suma = suma.add(nota);
		}
		BigDecimal cantidad = new BigDecimal(notas.size());
		return suma.divide(cantidad, 2, RoundingMode.HALF_DOWN);
	}
	public Integer obtenerCantidadAprobados() {
		Integer cantidad = 0;
		for (BigDecimal nota : notas.values()) {
			if (nota.compareTo(APROBADO) >= 0) {
				cantidad++;
			}
		}
		
		return cantidad;
	}
	
	public List<String> obtenerSuspensos(){
		List<String> suspensos = new ArrayList<>();
		for (String dni : notas.keySet()) {
			if (notas.get(dni).compareTo(APROBADO) < 0) {
				suspensos.add(dni);
			}
		}
		return suspensos;
	}
	public void borrarAprobados() {
		Iterator<String> it = notas.keySet().iterator();
		while (it.hasNext()) {
			String dni = (String) it.next();
			if (notas.get(dni).compareTo(APROBADO) >= 0) {
				it.remove();
			}
		}
		// ALTERNATIVA
//		List<String> suspensos = obtenerSuspensos();
//		Iterator<String> it = notas.keySet().iterator();
//		while (it.hasNext()) {
//			String dni = (String) it.next();
//			if (!suspensos.contains(dni)) {
//				it.remove();
//			}
//		}		
	}
	
	
	@Override
	public String toString() {
		DecimalFormat ft = new DecimalFormat("#,###.0");
		String aprobados = "Aprobados:\n";
		String suspensos = "Suspensos:\n";
		for (String dni : notas.keySet()) {
			String alumno = "\t" + dni + " (" + ft.format(notas.get(dni)) + ")\n";
			if (notas.get(dni).compareTo(APROBADO) < 0) {
				suspensos += alumno;
			}
			else {
				aprobados += alumno;
			}
		}
		return aprobados + "\n" + suspensos;
	
	}
	
	
	
}





