package ejercicios.ejercicio50;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class App {

	public static void main(String[] args) {
		Set<Pais> paises = new HashSet<>();
		System.out.println(paises);
		Pais pais1 = new Pais("ES", "Espa�a");
		Pais pais2 = new Pais("CK", "Islas Cook");
		Pais pais3 = new Pais("CK", "Islas Caim�n");
		paises.add(pais1);
		paises.add(pais2);
		paises.add(pais3);
		System.out.println(paises);
		
		Pais pais4 = new Pais("es", "Espa�a");
		paises.add(pais4);
		System.out.println(paises);

		for (Iterator<Pais> iterator = paises.iterator(); iterator.hasNext();) {
			Pais pais = (Pais) iterator.next();
			if (!pais.getCodigo().equalsIgnoreCase("es")) {
				iterator.remove();
			}
		}
		System.out.println(paises);
		paises.clear();
		System.out.println(paises);
	}

}



