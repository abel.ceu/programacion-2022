package ejercicios.repaso.ejercicio01;

public class Smartphone {

	private String marca;
	private String modelo;
	private String imei;
	private Double precio;
	private Integer aņoFabricacion;
	private Boolean activo;
	
	
	public Smartphone() {
		activo = true;
		precio = 0D;
	}
	public Smartphone(String imei) {
		activo = true;
		precio = 0D;
		this.imei = imei;
	}
	public Smartphone(String imei, String marca, String modelo) {
		activo = true;
		precio = 0D;
		this.imei = imei;
		this.marca = marca;
		this.modelo = modelo;
	}

	
	public void desactivar() {
		this.activo = false;
	}
	
	public void activar() {
		this.activo = true;
	}
	
	public Integer getEdad() {
		return aņoFabricacion - 2000;
	}
	
	public void cambiarMarcaModelo(String marca, String modelo) {
		this.marca = marca;
		this.modelo = modelo;
	}
	
	public void establecerFabricacion(Integer aņo) {
		aņoFabricacion = aņo;
	}
	
	public Double getPrecioMasIva() {
		return precio*1.21;
	}
	
	public Boolean isAltaGama(Double precioBase) {
		return precioBase < precio;
	}
	
	public Boolean isDatosCompletos() {
		return imei != null && !imei.isBlank() && precio != null;
	}
	
	public Boolean isGratis() {
		return precio == 0D;
	}
	
	public void rebajar() {
		if (precio < 10){
			precio = 0D;
		}
		else{
			precio = precio-10;
		}
	}
	
	
	@Override
	public String toString() {
		String resultado = marca + "(" + modelo + ")";
		if (!activo) {
			return resultado + " - INACTIVO";
		}
		return resultado;
	}
	
	
	
	
	
	
	
	
}
