package ejercicios.ejercicio57;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Ejercicio57 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		// Letras --> Lista de palabras
		Map<String, List<String>> mapa = new HashMap<String, List<String>>();
		String palabra;
		do {
			System.out.println("Dime una palabra");
			palabra = sc.nextLine();
			if (!palabra.equalsIgnoreCase("fin")) {
				String letra = palabra.substring(0, 1).toUpperCase();
				if (!mapa.containsKey(letra)) {
					List<String> palabras = new ArrayList<>();
					palabras.add(palabra);
					mapa.put(letra, palabras);
				} else {
					mapa.get(letra).add(palabra);
				}

			}
		} while (!palabra.equalsIgnoreCase("fin"));

		String letra;
		do {
			System.out.println("Dime una letra");
			letra = sc.nextLine().toUpperCase();
			if (letra.equals("FIN")) {
				break;
			}
			if (mapa.containsKey(letra)) {
				List<String> lista = mapa.get(letra);
				if (lista.size() > 1) {
					System.out.println("Hay " + lista.size() + " palabras que empiezan por " + letra + ":");
				} else {
					System.out.println("Hay 1 palabra que empieza por " + letra + ":");
				}
				for (String x : lista) {
					System.out.println("\t> " + x);
				}
			} else {
				System.out.println("No hay palabras con esa letra.");
			}
		} while (!letra.equals("FIN"));

		System.out.println("¡Gracias por jugar con nosotros! Bye");
		sc.close();
	}
}
