package ejercicios.ejercicio37;

import java.util.ArrayList;
import java.util.List;

public class ColaCadenas {
	protected List<String> cola;
	
	public ColaCadenas() {
		cola = new ArrayList<>();
	}
	
	public void aņadirCadena(String cadena) {
		cola.add(cadena);
	}
	
	public String sacarCadena() {
		if (cola.isEmpty()) {
			return null;
		}
		String masAntiguo = cola.get(0);
		cola.remove(0);
		return masAntiguo;
	}
	
	public Integer getTamaņo() {
		return cola.size();
	}
	
	@Override
	public String toString() {
		return cola.toString();
	}
	
	
}




