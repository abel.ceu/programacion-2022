package ejercicios.ejercicio49;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class Ejercicio49 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Set<String> conjunto = new HashSet<>();
		
		for (int i = 1; i <= 5; i++) {
			System.out.println("Dame la cadena n�mero " + i);
			String cadena = sc.nextLine();
			conjunto.add(cadena);
		}
		System.out.println(conjunto);

		Set<String> temporal = new HashSet<>();
		for (String cadena : conjunto) {
			temporal.add(cadena.toUpperCase());
		}
		conjunto = temporal;
		System.out.println(conjunto);
		
		if (conjunto.contains("")) {
			System.out.println("La lista contiene alguna cadena vac�a");
		}
		else {
			System.out.println("No hay cadenas vac�a");
		}
		
//		for (Iterator<String> it = lista.iterator(); it.hasNext();) {
//			String siguiente = it.next();
//			if (siguiente.length()<6) {
//				it.remove();
//			}
//		}
		
		Iterator<String> it = conjunto.iterator();
		while(it.hasNext()) {
			String siguiente = it.next();
			if (siguiente.length()<6) {
				it.remove();
			}
		}
		System.out.println(conjunto);
		
		
		sc.close();
	}
}






