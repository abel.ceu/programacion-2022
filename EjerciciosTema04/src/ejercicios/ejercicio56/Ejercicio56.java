package ejercicios.ejercicio56;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Ejercicio56 {

	public static void main(String[] args) {
		Map<Integer, Integer> mapa = new HashMap<>();
		Scanner sc = new Scanner(System.in);
		Integer numero;
		Integer cantidad = 0;
		do {
			System.out.println("Dime un n�mero");
			numero = sc.nextInt();
			if (numero != 0) {
				cantidad++;
				if (!mapa.containsKey(numero)) {
					mapa.put(numero, 1);
				}
				else {
					Integer nuevaCant = mapa.get(numero) + 1;
					mapa.put(numero, nuevaCant);
				}
			}
//			if (numero != 0) { ALTERNATIVA
//				cantidad++;
//				Integer nuevaCant = mapa.getOrDefault(numero, 0) + 1;
//				mapa.put(numero, nuevaCant);
//			}
		} while (numero!=0);
		
		sc.close();
		
		System.out.println("Total n�meros indicados: " + cantidad);
		System.out.println("Distribuci�n:");
		for (Integer key : mapa.keySet()) {
			String veces = " veces";
			if (mapa.get(key) == 1) {
				veces = " vez";
			}
			System.out.println("\t> N�mero " + key + ": " + mapa.get(key) + veces);
		}
		
		
	}

}
