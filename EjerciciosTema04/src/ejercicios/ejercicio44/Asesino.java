package ejercicios.ejercicio44;

public class Asesino extends Personaje{

	public Asesino() {
		vida = 100;
	}
	
	@Override
	public Integer getDaņo() {
		return 10;
	}

	@Override
	public Integer getPeriodoSegundosDaņo() {
		return 5;
	}

	
}
