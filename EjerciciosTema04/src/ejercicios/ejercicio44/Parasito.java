package ejercicios.ejercicio44;

public class Parasito extends Personaje {

	public Parasito() {
		vida = 200;
	}
	
	@Override
	public Integer getDaņo() {
		return 2;
	}

	@Override
	public Integer getPeriodoSegundosDaņo() {
		return 1;
	}

}
