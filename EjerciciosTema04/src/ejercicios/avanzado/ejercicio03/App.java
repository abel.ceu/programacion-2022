package ejercicios.avanzado.ejercicio03;

import java.util.ArrayList;
import java.util.List;

public class App {
	public static void main(String[] args) {
		Biblioteca biblioteca = new Biblioteca();
		Libro libro = new Libro("A32423", "Nietzsche", "As� habl� Zaratustra", 1881);
		Revista revista = new Revista("DAAAA", "Abel Morillo", "Los males de la programaci�n", 2023);
		Cd cd = new Cd("83833X", "The class", "London Calling", 1979);
		
		List<Publicacion> lista = biblioteca.getPublicaciones();
		lista.add(revista);
		lista.add(libro);
		lista.add(cd);
		
		libro.prestar();
		cd.prestar();
		libro.retirarParaConsultar();
		revista.retirarParaConsultar();
		for (Publicacion publicacion : lista) {
			System.out.println(publicacion);
		}
		
		libro.devolver();
		libro.retirarParaConsultar();
		libro.prestar();
		for (Publicacion publicacion : lista) {
			System.out.println(publicacion);
		}
		cd.devolver();
		libro.terminarConsulta();
		revista.terminarConsulta();
		for (Publicacion publicacion : lista) {
			System.out.println(publicacion);
		}
		
		
		
		
				
	}
}
