package ejercicios.avanzado.ejercicio03;

import java.util.Objects;

public abstract class Publicacion {
	private String codigo;
	private String autor;
	private String titulo;
	private Integer aņoPublicacion;
	
	public Publicacion(String codigo, String autor, String titulo, Integer aņoPublicacion) {
		super();
		this.codigo = codigo;
		this.autor = autor;
		this.titulo = titulo;
		this.aņoPublicacion = aņoPublicacion;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Integer getAņoPublicacion() {
		return aņoPublicacion;
	}
	public void setAņoPublicacion(Integer aņoPublicacion) {
		this.aņoPublicacion = aņoPublicacion;
	}
	@Override
	public String toString() {
		return "[codigo=" + codigo + ", autor=" + autor + ", titulo=" + titulo + ", aņoPublicacion="
				+ aņoPublicacion;
	}
	@Override
	public int hashCode() {
		return Objects.hash(codigo);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Publicacion other = (Publicacion) obj;
		return Objects.equals(codigo, other.codigo);
	}
	
	
	
}
