package ejercicios.avanzado.ejercicio03;

public class Revista extends Publicacion implements IConsultable {

	private Boolean enConsulta;

	public Revista(String codigo, String autor, String titulo, Integer aņoPublicacion) {
		super(codigo, autor, titulo, aņoPublicacion);
		enConsulta = false;
	}
	
	@Override
	public void retirarParaConsultar() {
		enConsulta = true;

	}

	@Override
	public void terminarConsulta() {
		enConsulta = false;

	}

	@Override
	public Boolean estaConsultando() {
		return enConsulta;
	}
	
	@Override
	public String toString() {
		return super.toString() + " EnConsulta=" + enConsulta + "]";
	}
	
}
