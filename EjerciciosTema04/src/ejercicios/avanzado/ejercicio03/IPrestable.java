package ejercicios.avanzado.ejercicio03;

public interface IPrestable {

	public void prestar();
	public void devolver();
	public Boolean estaPrestado();
	
	
}
