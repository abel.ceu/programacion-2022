package ejercicios.avanzado.ejercicio03;

public class Libro extends Publicacion implements IPrestable, IConsultable{

	private Boolean prestado;
	private Boolean enConsulta;
	
	public Libro(String codigo, String autor, String titulo, Integer añoPublicacion) {
		super(codigo, autor, titulo, añoPublicacion);
		enConsulta = false;
		prestado = false;
	}
	
	@Override
	public void retirarParaConsultar() {
		if (!prestado) {
			enConsulta = true;
		}
		
	}
	@Override
	public void terminarConsulta() {
		enConsulta = false;
		
	}
	@Override
	public Boolean estaConsultando() {
		return enConsulta;
	}
	@Override
	public void prestar() {
		if (!estaConsultando()) {
			prestado = true;
		}
//		if (!enConsulta) { // igual sin llamar al método
//			prestado = true;
//		}
		
	}
	@Override
	public void devolver() {
		prestado = false;
		
	}
	@Override
	public Boolean estaPrestado() {
		return prestado;
	}
	
	
	@Override
	public String toString() {
		return super.toString() + " Prestado=" + prestado + ", EnConsulta=" + enConsulta + "]";
	}
	
}
