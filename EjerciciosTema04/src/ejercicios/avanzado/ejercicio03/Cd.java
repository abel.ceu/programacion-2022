package ejercicios.avanzado.ejercicio03;

public class Cd extends Publicacion implements IPrestable {

	
	private Boolean prestado;

	public Cd(String codigo, String autor, String titulo, Integer aņoPublicacion) {
		super(codigo, autor, titulo, aņoPublicacion);
		prestado = false;
	}
	
	@Override
	public void prestar() {
		prestado = true;
	}

	@Override
	public void devolver() {
		prestado = false;

	}

	@Override
	public Boolean estaPrestado() {
		return prestado;
	}
	
	@Override
	public String toString() {
		return super.toString() + " Prestado=" + prestado + "]";
	}
}
