package ejercicios.avanzado.ejercicio03;

public interface IConsultable {

	public void retirarParaConsultar();
	public void terminarConsulta();
	public Boolean estaConsultando();
	
}
