package ejercicios.avanzado.ejercicio03;

import java.util.ArrayList;
import java.util.List;

public class Biblioteca {
	private List<Publicacion> publicaciones;

	
	public Biblioteca() {
		publicaciones = new ArrayList<>();
	}
	
	public List<Publicacion> getPublicaciones() {
		return publicaciones;
	}

	public void setPublicaciones(List<Publicacion> publicaciones) {
		this.publicaciones = publicaciones;
	}
	
	
}
