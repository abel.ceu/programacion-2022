package ejercicios.avanzado.ejercicio01y02;

public class Fijo extends Telefono{
	private String direccion;
	
	public Fijo(Integer numero, String direccion) {
		this.direccion = direccion;
		setNumero(numero);
		setEnLlamada(false);
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
}
