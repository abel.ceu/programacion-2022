package ejercicios.avanzado.ejercicio01y02;

public class Movil extends Telefono{
	private Gps gps;
	
	public Movil(Integer numero, Gps gps) {
		this.gps = gps;
		setNumero(numero);
		setEnLlamada(false);
	}

	public Gps getGps() {
		return gps;
	}

	public void setGps(Gps gps) {
		this.gps = gps;
	}
	
	
}
