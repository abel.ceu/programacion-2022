package ejercicios.avanzado.ejercicio01y02;

public abstract class Telefono {
	private Integer numero;
	private Boolean enLlamada;
	
	public void marcar(Integer numero) {
		if (numero == null) {
			return;
		}
		System.out.println("LLAMANDO A... " + numero);
		if (this.numero.equals(numero)) {
			System.out.println("COMUNICANDO");
		}
		else {
			System.out.println("EN COMUNICACIÓN");
			enLlamada = true;
		}
	}
	
	public void colgar() {
		if (enLlamada) {
			System.out.println("COMUNICACIÓN TERMINADA");
			enLlamada = false;
		}
	}
	
	public Integer getNumero() {
		return numero;
	}
	public Integer consultarNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public Boolean getEnLlamada() {
		return enLlamada;
	}
	public void setEnLlamada(Boolean enLlamada) {
		this.enLlamada = enLlamada;
	}
	
	
	
	
}
