package ejercicios.avanzado.ejercicio01y02;

import java.math.BigDecimal;

public class App {
	public static void main(String[] args) {
		Gps gps = new Gps(new BigDecimal("39.3223232"), new BigDecimal("-32.4848343"));
		Movil m = new Movil(654654654, gps);
		Fijo f = new Fijo(954954954, "Avda. de los sue�os azules, 32");
		
		System.out.println(m.consultarNumero());
		System.out.println(f.consultarNumero());
		
		m.marcar(654654654);
		m.marcar(610610610);
		
		m.colgar();
		m.colgar();
	
	}
}
