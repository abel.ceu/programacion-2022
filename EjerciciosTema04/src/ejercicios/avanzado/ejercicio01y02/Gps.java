package ejercicios.avanzado.ejercicio01y02;

import java.math.BigDecimal;

public class Gps {
	private BigDecimal gpsLatitud;
	private BigDecimal gpsLongitud;
	
	
	
	public Gps(BigDecimal gpsLatitud, BigDecimal gpsLongitud) {
		super();
		this.gpsLatitud = gpsLatitud;
		this.gpsLongitud = gpsLongitud;
	}
	public BigDecimal getGpsLatitud() {
		return gpsLatitud;
	}
	public void setGpsLatitud(BigDecimal gpsLatitud) {
		this.gpsLatitud = gpsLatitud;
	}
	public BigDecimal getGpsLongitud() {
		return gpsLongitud;
	}
	public void setGpsLongitud(BigDecimal gpsLongitud) {
		this.gpsLongitud = gpsLongitud;
	}
	
	
}
