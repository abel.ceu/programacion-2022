package ejercicios.ejercicio43;

public abstract class Trabajador {
	private String nombre;
	private Integer aņoNacimiento;
	private String nacionalidad;
	
	public Trabajador(String nombre, Integer aņoNacimiento, String nacionalidad) {
		super();
		this.nombre = nombre;
		this.aņoNacimiento = aņoNacimiento;
		this.nacionalidad = nacionalidad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getAņoNacimiento() {
		return aņoNacimiento;
	}
	public void setAņoNacimiento(Integer aņoNacimiento) {
		this.aņoNacimiento = aņoNacimiento;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	
	public abstract Integer getSueldo();
	
	@Override
	public String toString() {
		return "Trabajador [nombre=" + nombre + ", aņoNacimiento=" + aņoNacimiento + ", nacionalidad=" + nacionalidad
				+ "]";
	}
	
	
	
}
