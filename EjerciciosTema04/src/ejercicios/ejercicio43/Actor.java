package ejercicios.ejercicio43;

public class Actor extends Trabajador {

	public Actor(String nombre, Integer aņoNacimiento, String nacionalidad) {
		super(nombre, aņoNacimiento, nacionalidad);
		
	}

	private static final int SUELDO = 100000;
	
	@Override
	public Integer getSueldo() {
		return SUELDO;
	}
	
	
	
}
