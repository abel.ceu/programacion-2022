package ejercicios.ejercicio43;

public class Guionista extends Trabajador {

	private static final int SUELDO = 50000;

	public Guionista(String nombre, Integer aņoNacimiento, String nacionalidad) {
		super(nombre, aņoNacimiento, nacionalidad);
		
	}
	
	@Override
	public Integer getSueldo() {
		return SUELDO;
	}
}
