package ejercicios.ejercicio43;

public class Director extends Trabajador {

	private static final int SUELDO = 100000;

	public Director(String nombre, Integer aņoNacimiento, String nacionalidad) {
		super(nombre, aņoNacimiento, nacionalidad);
		
	}
	
	@Override
	public Integer getSueldo() {
		return SUELDO;
	}

}
