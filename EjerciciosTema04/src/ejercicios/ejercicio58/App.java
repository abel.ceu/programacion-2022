package ejercicios.ejercicio58;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		List<String> palabras = new ArrayList<>();
		String palabra;
		do {
			System.out.println("Dime una palabra");
			palabra = sc.nextLine();
			if (!palabra.equalsIgnoreCase("fin")) {
				palabras.add(palabra);
			}
		} while (!palabra.equalsIgnoreCase("fin"));

		// Crear un diccionario y cargarlo con la lista
		Diccionario diccionario = new Diccionario();
		diccionario.cargarDiccionario(palabras);
		
		String letra;
		do {
			System.out.println("Dime una letra");
			letra = sc.nextLine().toUpperCase();
			if (letra.equals("FIN")) {
				break;
			}
			// pedirle al diccionario que imprima las palabras de esa letra
			diccionario.imprimirPalabras(letra);
		} while (!letra.equals("FIN"));

		System.out.println("¡Gracias por jugar con nosotros! Bye");
		sc.close();
	}
}
