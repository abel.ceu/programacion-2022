package ejercicios.ejercicio58;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Diccionario {

	private Map<String, List<String>> mapa;

	public Diccionario() {
		mapa = new HashMap<>();
	}
	
	public void cargarDiccionario(List<String> palabras) {
		for (String palabra : palabras) {
			String letra = palabra.substring(0, 1).toUpperCase();
			if (!mapa.containsKey(letra)) {
				mapa.put(letra, new ArrayList<>());
			} 
			mapa.get(letra).add(palabra);
		}
	}

	public void borrarDiccionario() {
		mapa.clear();
	}

	public void imprimirPalabras(String letra) {
		if (mapa.containsKey(letra)) {
			List<String> lista = mapa.get(letra);
			if (lista.size() > 1) {
				System.out.println("Hay " + lista.size() + " palabras que empiezan por " + letra + ":");
			} else {
				System.out.println("Hay 1 palabra que empieza por " + letra + ":");
			}
			for (String x : lista) {
				System.out.println("\t> " + x);
			}
		} else {
			System.out.println("No hay palabras con esa letra.");
		}
	}

	@Override
	public String toString() {
		return "Diccionario " + mapa;
	}
	
	

}
