package ejercicios.ejercicio45;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Ejercicio45 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		LocalDate fechaNacimiento;
		LocalDate hoy = LocalDate.now();
		do{
			System.out.println("Dame tu fecha de nacimiento. Empieza por el d�a:");
			Integer dia = sc.nextInt();
			System.out.println("Ahora el mes:");
			Integer mes = sc.nextInt();
			System.out.println("Ahora el a�o:");
			Integer a�o = sc.nextInt();
			fechaNacimiento = LocalDate.of(a�o, mes, dia);
			if (!fechaNacimiento.isBefore(hoy)) {
				System.out.println("Todav�a no has nacido?? Int�ntalo de nuevo");
			}
		}
		while(!fechaNacimiento.isBefore(hoy));
		
		if (fechaNacimiento.isLeapYear()) {
			System.out.println("Naciste en un a�o bisiesto");
		}
		System.out.println("Naciste un " + fechaNacimiento.getDayOfWeek());
		
		Period periodo = fechaNacimiento.until(hoy);
		System.out.println("Tienes " + periodo.getYears() + " a�os");
		
		LocalDate fechaDeceso = fechaNacimiento.plusYears(100);
		periodo = hoy.until(fechaDeceso);
		System.out.println("Te quedan " + periodo.getYears() + " a�os, " + periodo.getMonths() + " meses y "+ periodo.getDays() + " d�as");
		
		LocalTime hora = LocalTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
		System.out.println("La hora es " + hora.format(formatter));
		
		sc.close();
	}
	
}




